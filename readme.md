# ETFS
ETFS stands for "extendable tape file system" and is one of the two core components of the notaping project. The goal
is to provide a file system specialised for linear data storage which can be extended with custom data structures.

## What is the notaping project?
The notaping project aims to provide an open-source and easy to use personal tape backup software. For more information
please see the [project page](#TODO).

## What does "extendable" mean?
Per default, ETFS only stores 3 types of data:
- Some metadata (e.g. magic, version, labels, plugin info, etc.)
- The "table of contents" (Basically the file list)
- File contents

As can be seen, this is only good for storing files and keeping their structure, but not any metadata (e.g. permissions).

To add such functionality, custom code can be added in the form of plugins which will serialize their own data structures
to the tape. THis can be used to add things like permission information or extend the file list to contain information
about files present in archives.
