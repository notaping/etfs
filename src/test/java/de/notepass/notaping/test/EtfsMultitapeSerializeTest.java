package de.notepass.notaping.test;

import de.notepass.notaping.core.fs.etfs.action.model.FileSelector;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.test.callback.MultitapeValidatingProgressCallback;
import de.notepass.notaping.test.callback.NoopReadProgressCallback;
import de.notepass.notaping.test.callback.NoopWriteProgressCallback;
import de.notepass.notaping.test.callback.TemporaryFilePathRemapper;
import de.notepass.notaping.core.access.jTapeAccess.impl.mock.FileMockedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsReadDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsWriteDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.test.opMapper.MockImplementaionOpMaper;
import org.junit.*;
import de.notepass.notaping.test.plugin.TestPlugin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Tests multitape support and TapeDrive connectivity
 */
public class EtfsMultitapeSerializeTest {
    private FileMockedTapeDrive tapeDrive;
    private EtfsInstance writeInstance;
    private EtfsInstance readInstance;
    private EtfsInstance skippingReadInstance;
    private TemporaryFilePathRemapper tempFilePathRemapper = new TemporaryFilePathRemapper();
    private static final int TAPE_SIZE_BYTES = 256 * 1024;
    private static final int TAPE_INPUT_FILE_SIZE_BYTES = 64 * 1024;
    private List<Path> inputFiles;
    private int numInputFiles = 20;
    private static List<Path> tapeFiles;
    private int tapeFileIndex = 0;

    @BeforeClass
    public static void prepareGlobal() throws IOException {
        tapeFiles = new ArrayList<>();
        tapeFiles.add(Files.createTempFile("TAPE-FILE-", ".tmp"));

        // Set the buffer to an acceptable size for the test
        System.setProperty("etfs.core.buffer.write.tape.size", String.valueOf(TAPE_SIZE_BYTES / 2));
    }

    @Before
    public void prepare() throws IOException {
        inputFiles = new ArrayList<>();
        byte[] buffer = new byte[TAPE_INPUT_FILE_SIZE_BYTES];
        int neededTapeFiles = (int) Math.ceil(((double) (numInputFiles * TAPE_INPUT_FILE_SIZE_BYTES)) / ((double) TAPE_SIZE_BYTES));
        int tapeFileCap = neededTapeFiles * TAPE_SIZE_BYTES;
        int fileCap = numInputFiles * TAPE_INPUT_FILE_SIZE_BYTES;

        System.out.println("Will need about " + neededTapeFiles + " virtual tapes to provide " + (tapeFileCap / 1024) + "KiB of storage for " + (fileCap / 1024) + "KiB of file contents (" + numInputFiles + " files @ " + (TAPE_INPUT_FILE_SIZE_BYTES / 1024) + "KiB)");

        for (int i = 0; i < numInputFiles; i++) {
            Path file = Files.createTempFile("TAPE-INPUT-FILE-", ".tmp");
            inputFiles.add(file);

            //TODO: [Low] Better data generation. This will only generate 10 different files, but input is 20
            Arrays.fill(buffer, (byte) String.valueOf(i).toCharArray()[0]);
            Files.write(file, buffer);
        }

        writeInstance = EtfsWriteDirectionBuilder.newInstance()
                .withTapeSpecificOperationDispatcher(new MockImplementaionOpMaper(true, TAPE_SIZE_BYTES, tapeFiles.get(tapeFileIndex), FileMockedTapeDrive.WORK_DIRECTION.WRITE))
                .addFiles(inputFiles)
                .addPlugin(new TestPlugin())
                .withLabel("MULTITAPE_TEST_VOLUME")
                .build();

        readInstance = EtfsReadDirectionBuilder.newInstance(tempFilePathRemapper)
                .withTapeSpecificOperationDispatcher(new MockImplementaionOpMaper(true, TAPE_SIZE_BYTES))
                .addPlugin(new TestPlugin())
                .build();

        skippingReadInstance = EtfsReadDirectionBuilder.newInstance(tempFilePathRemapper)
                .withTapeSpecificOperationDispatcher(new MockImplementaionOpMaper(true, TAPE_SIZE_BYTES))
                .addPlugin(new TestPlugin())
                .withFileSelector(new FileSelector() {
                    String lastName = null;

                    @Override
                    public void prepareFileSelection(List<FileEntry> allFiles) {
                        lastName = allFiles.get(allFiles.size() - 1).getFilename();
                    }

                    @Override
                    public boolean writeFile(FileEntry file) {
                        return file.getFilename().equals(lastName);
                    }
                })
                .build();

        tapeDrive = ((MockImplementaionOpMaper) writeInstance.getOpDispatcher()).getTapeDrive();
    }

    public void testWriteMultitape() throws IOException {
        try {
            System.out.println("Using tape file: " + tapeDrive.getTapeDriveFile());
            writeInstance.serialize(tapeDrive.getOutputStream(), new NoopWriteProgressCallback() {
                @Override
                public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int nextTapeIndex) throws IOException {
                    tapeFiles.add(Files.createTempFile("TAPE-FILE-", ".tmp"));
                    tapeFileIndex++;
                    tapeDrive.ejectTape();

                    // Check amount of bytes written. Must equals available tape size
                    long tapeFileSize = Files.size(tapeFiles.get(tapeFileIndex - 1));
                    Assert.assertEquals("Amount of written data (" + (tapeFileSize / 1024) + "KiB) does not equal virtual tape size (" + (TAPE_SIZE_BYTES / 1024) + "KiB)", TAPE_SIZE_BYTES, tapeFileSize);

                    tapeDrive.setTapeDriveFile(tapeFiles.get(tapeFileIndex).toString());
                    tapeDrive.setWorkDirection(FileMockedTapeDrive.WORK_DIRECTION.WRITE);
                    tapeDrive.loadTape();
                    System.out.println("Using tape file: " + tapeDrive.getTapeDriveFile());
                    return MultitapeAction.TAPE_CHANGED;
                }
            });
        } finally {
            tapeDrive.ejectTape();
            long tapeFileSize = Files.size(tapeFiles.get(tapeFileIndex));
            Assert.assertTrue("Amount of written data (" + (tapeFileSize / 1024) + "KiB) exceeds virtual tape size (" + (TAPE_SIZE_BYTES / 1024) + "KiB)", TAPE_SIZE_BYTES >= tapeFileSize);
        }
    }

    @Test
    public void testReadMultitape() throws IOException {
        // Execute write test as we depend on it
        testWriteMultitape();

        // Switch virtual tape devices and reset tape file index for reading
        tapeFileIndex = 0;
        int inputFileStartIndex = 0;
        tapeDrive = ((MockImplementaionOpMaper) readInstance.getOpDispatcher()).getTapeDrive();
        tapeDrive.ejectTape();
        tapeDrive.setTapeDriveFile(tapeFiles.get(0).toString());
        tapeDrive.setWorkDirection(FileMockedTapeDrive.WORK_DIRECTION.READ);
        tapeDrive.loadTape();

        System.out.println("Reading tape file " + tapeDrive.getTapeDriveFile());

        readInstance.deserialize(tapeDrive.getInputStream(),
                new MultitapeValidatingProgressCallback(
                        inputFileStartIndex,
                        tapeFileIndex,
                        numInputFiles,
                        TAPE_INPUT_FILE_SIZE_BYTES,
                        tempFilePathRemapper,
                        tapeDrive,
                        writeInstance,
                        tapeFiles
                )
        );

        System.out.println("Reading with skipping across tape boundaries");
        tapeDrive = ((MockImplementaionOpMaper) skippingReadInstance.getOpDispatcher()).getTapeDrive();
        tapeDrive.ejectTape();
        tapeDrive.setTapeDriveFile(tapeFiles.get(0).toString());
        tapeDrive.setWorkDirection(FileMockedTapeDrive.WORK_DIRECTION.READ);
        tapeDrive.loadTape();
        inputFileStartIndex = inputFiles.size() - 1;
        skippingReadInstance.deserialize(tapeDrive.getInputStream(),
                new MultitapeValidatingProgressCallback(
                        inputFileStartIndex,
                        tapeFileIndex,
                        numInputFiles,
                        TAPE_INPUT_FILE_SIZE_BYTES,
                        tempFilePathRemapper,
                        tapeDrive,
                        writeInstance,
                        tapeFiles
                )
        );
    }

    @After
    public void deleteTempFiles() throws IOException {
        for (Path p : inputFiles) {
            Files.delete(p);
        }

        for (Path p : tapeFiles) {
            Files.delete(p);
            Files.delete(Path.of(p.toString()+".eof.properties"));
        }
    }
}
