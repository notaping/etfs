package de.notepass.notaping.test.plugin;

import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileContent;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

public class TestPlugin extends EtfsPlugin {
    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    public String getID() {
        return "TEST_PLUGIN";
    }

    @Override
    public boolean wantsToWriteRequiredConfigForReadOperation() {
        return true;
    }

    @Override
    public void writeRequiredConfigForReadOperation(OutputStream os) throws IOException {
        os.write(SerializeUtils.toBytes("TEST_PLUGIN_CONFIG_STRING"));
    }

    @Override
    public void readRequiredConfigForReadOperation(InputStream is, int version, int dataSize) throws IOException {
        is.read(new byte[dataSize]);
        // NOOP
    }

    @Override
    public boolean wantsToWritePluginData(MetadataPosition metadataPosition, HeaderSection context) {
        return true;
    }

    @Override
    public void writePluginData(MetadataPosition metadataPosition, HeaderSection context, OutputStream os) throws IOException {
        os.write(SerializeUtils.toBytes("TEST_PLUGIN_DATA[@"+metadataPosition+"]"));
    }

    @Override
    public void readPluginData(MetadataPosition metadataPosition, HeaderSection context, InputStream is, int version, int dataSize) throws IOException {
        is.read(new byte[dataSize]);
        // NOOP
    }

    @Override
    public boolean wantsToWritePluginData(MetadataPosition metadataPosition, FileEntry context) {
        return true;
    }

    @Override
    public void writePluginData(MetadataPosition metadataPosition, FileEntry context, OutputStream os) throws IOException {
        os.write(SerializeUtils.toBytes("TEST_PLUGIN_DATA[@"+metadataPosition+"]"));
    }

    @Override
    public void readPluginData(MetadataPosition metadataPosition, FileEntry context, InputStream is, int version, int dataSize) throws IOException {
        is.read(new byte[dataSize]);
        // NOOP
    }

    @Override
    public boolean wantsToWritePluginData(MetadataPosition metadataPosition, FileContent context, Path file) {
        return true;
    }

    @Override
    public void writePluginData(MetadataPosition metadataPosition, FileContent context, Path file, OutputStream os) throws IOException {
        os.write(SerializeUtils.toBytes("TEST_PLUGIN_DATA[@"+metadataPosition+"]"));
    }

    @Override
    public void readPluginData(MetadataPosition metadataPosition, FileContent context, Path file, InputStream is, int version, int dataSize) throws IOException {
        is.read(new byte[dataSize]);
        // NOOP
    }
}
