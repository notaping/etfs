package de.notepass.notaping.test.callback;

import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.io.IOException;
import java.nio.file.Path;
import java.util.UUID;

public class NoopWriteProgressCallback implements EtfsWriteProgressCallback {
    @Override
    public long getReportFileWriteProgressBytes() {
        return DEFAULT_FILE_PROGRESS_REPORT_SIZE;
    }

    @Override
    public long getReportOverallFileWriteProgressBytes() {
        return DEFAULT_OVERALL_PROGRESS_REPORT_SIZE;
    }

    @Override
    public void onStartWriteHeaderSection(HeaderSection headerSection) {

    }

    @Override
    public void onEndWriteHeaderSection(HeaderSection headerSection) {

    }

    @Override
    public void onStartWritePluginConfigSection(PluginConfigSection pluginConfigSection) {

    }

    @Override
    public void onEndWritePluginConfigSection(PluginConfigSection pluginConfigSection) {

    }

    @Override
    public void onStartWritePluginInfo(EtfsPlugin plugin) {

    }

    @Override
    public void onEndWritePluginInfo(EtfsPlugin plugin) {

    }

    @Override
    public void onStartWritePluginConfiguration(EtfsPlugin plugin) {

    }

    @Override
    public void onEndWritePluginConfiguration(EtfsPlugin plugin) {

    }

    @Override
    public void onStartWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {

    }

    @Override
    public void onStartWriteTableOfContentsEntry(FileEntry entry) {

    }

    @Override
    public void onEndWriteTableOfContentsEntry(FileEntry entry) {

    }

    @Override
    public void onEndWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {

    }

    @Override
    public void onStartWriteFileSection(long entries) {

    }

    @Override
    public void onEndWriteFileSection() {

    }

    @Override
    public void onStartWriteFile(Path target, long fileSize) {

    }

    @Override
    public void onEndWriteFile(Path target, long fileSize) {

    }

    @Override
    public void onUnreadableFile(Path target, IOException e) {

    }

    @Override
    public void onFileProgress(Path target, long currentPosition, long fileSize) {

    }

    @Override
    public void onStartWritePluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries) {

    }

    @Override
    public void onEndWritePluginCustomDataSection(MetadataPosition metadataPosition) {

    }

    @Override
    public void onStartWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {

    }

    @Override
    public void onEndWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {

    }

    @Override
    public void onWriteAbort() {

    }

    @Override
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e) {

    }

    @Override
    public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int nextTapeIndex) throws IOException {
        return MultitapeAction.ABORT;
    }

    @Override
    public void onOverallFileProgress(long bytesWritten, long overallBytesToWrite) {

    }
}
