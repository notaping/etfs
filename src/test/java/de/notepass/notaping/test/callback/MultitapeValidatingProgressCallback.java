package de.notepass.notaping.test.callback;

import de.notepass.notaping.core.access.jTapeAccess.impl.mock.FileMockedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.ExtendedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import org.junit.Assert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MultitapeValidatingProgressCallback extends NoopReadProgressCallback {
    public int inputFileIndex = 0;
    public int tapeFileIndex = 0;
    public final int numInputFiles;
    public final int TAPE_INPUT_FILE_SIZE_BYTES;
    public final TemporaryFilePathRemapper tempFilePathRemapper;
    public final FileMockedTapeDrive tapeDrive;
    public final EtfsInstance writeInstance;
    public final List<Path> tapeFiles;


    public MultitapeValidatingProgressCallback(int inputFileIndex, int tapeFileIndex, int numInputFiles, int TAPE_INPUT_FILE_SIZE_BYTES, TemporaryFilePathRemapper tempFilePathRemapper, FileMockedTapeDrive tapeDrive, EtfsInstance writeInstance, List<Path> tapeFiles) {
        this.inputFileIndex = inputFileIndex;
        this.tapeFileIndex = tapeFileIndex;
        this.numInputFiles = numInputFiles;
        this.TAPE_INPUT_FILE_SIZE_BYTES = TAPE_INPUT_FILE_SIZE_BYTES;
        this.tempFilePathRemapper = tempFilePathRemapper;
        this.tapeDrive = tapeDrive;
        this.writeInstance = writeInstance;
        this.tapeFiles = tapeFiles;
    }

    @Override
    public void onStartReadFile(Path target, long fileSize) {
        System.out.println("Starting to read file " + (inputFileIndex) + " of " + (numInputFiles - 1));
    }

    @Override
    public void onEndReadFile(Path target, long fileSize) {
        System.out.println("Finished to read file " + (inputFileIndex) + " of " + (numInputFiles - 1));
        byte[] expectedContent = new byte[TAPE_INPUT_FILE_SIZE_BYTES];
        Arrays.fill(expectedContent, (byte) String.valueOf(inputFileIndex).toCharArray()[0]);
        byte[] actualContent = null;
        try {
            actualContent = Files.readAllBytes(tempFilePathRemapper.getRemappedFiles().get(inputFileIndex));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Encountered error while trying to read temporary file content");
        }

        Assert.assertArrayEquals("Actual file content doesn't match expected file content", expectedContent, actualContent);

        inputFileIndex++;
    }

    @Override
    public void onMissingPluginDiscovered(String pluginId) {
        Assert.fail("Found missing plugin with ID: " + pluginId);
    }

    @Override
    public void onUnreadableFile() {
        Assert.fail("Found unreadable file");
    }

    @Override
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e) {
        Assert.fail("Unexpected plugin error: " + e.getMessage());
    }

    @Override
    public void onEndReadHeaderSection(HeaderSection headerSection) {
        Assert.assertEquals("Wrong tape volume label", writeInstance.getLogicalTapeInfo().getHeaderSection().getLabel(), headerSection.getLabel());
        Assert.assertEquals("Wrong multitape index", tapeFileIndex, headerSection.getMultitapeIndex());
        Assert.assertEquals("Wrong tape volume label", writeInstance.getLogicalTapeInfo().getHeaderSection().getVolumeId(), headerSection.getVolumeId());
        Assert.assertEquals("Wrong tape volume label", writeInstance.getLogicalTapeInfo().getHeaderSection().getVersion(), headerSection.getVersion());
        Assert.assertEquals("Wrong tape volume label", writeInstance.getLogicalTapeInfo().getHeaderSection().getMinVersion(), headerSection.getMinVersion());
    }

    @Override
    public void onEndReadTableOfContentSection(TableOfContentSection tableOfContentSection) {
        Assert.assertEquals("The wrong amount of files on the tape has been read", writeInstance.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().size(), tableOfContentSection.getFileEntries().size());
    }

    @Override
    public void onEndReadPluginConfigSection(PluginConfigSection pluginConfigSection) {
        Assert.assertEquals("More plugin config entries where found in the plugin config section than expected", writeInstance.getLogicalTapeInfo().getPluginConfigSection().getEntries().size(), pluginConfigSection.getEntries().size());
        Assert.assertEquals("More plugin custom data entries where found in the plugin config section than expected", writeInstance.getLogicalTapeInfo().getPluginConfigSection().getPluginData().size(), pluginConfigSection.getPluginData().size());
    }

    @Override
    public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int expectedTapeIndex) throws TapeException {
        tapeDrive.ejectTape();
        tapeDrive.setTapeDriveFile(tapeFiles.get(expectedTapeIndex).toString());
        tapeDrive.setWorkDirection(FileMockedTapeDrive.WORK_DIRECTION.READ);
        tapeDrive.loadTape();

        System.out.println("Reading tape file " + tapeDrive.getTapeDriveFile());

        tapeFileIndex = expectedTapeIndex;

        return MultitapeAction.TAPE_CHANGED;
    }
}
