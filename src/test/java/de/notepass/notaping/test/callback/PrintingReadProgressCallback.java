package de.notepass.notaping.test.callback;

import de.notepass.notaping.core.fs.etfs.EtfsException;
import de.notepass.notaping.core.fs.etfs.data.ReadAbortReason;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.nio.file.Path;
import java.util.UUID;

public class PrintingReadProgressCallback implements EtfsReadProgressCallback {
    @Override
    public long getReportFileWriteProgressBytes() {
        return 64;
    }

    @Override
    public void onStartReadHeaderSection() {
        System.out.println("onStartReadHeaderSection");
    }

    @Override
    public void onEndReadHeaderSection(HeaderSection headerSection) {
        System.out.println("onEndReadHeaderSection");
    }

    @Override
    public void onStartReadPluginConfigSection(short numEntries) {
        System.out.println("onStartReadPluginConfigSection");
    }

    @Override
    public void onEndReadPluginConfigSection(PluginConfigSection pluginConfigSection) {
        System.out.println("onEndReadPluginConfigSection");
    }

    @Override
    public void onPluginDiscovered(EtfsPlugin plugin) {
        System.out.println("onPluginDiscovered");
    }

    @Override
    public void onPluginReconfigured(EtfsPlugin plugin) {
        System.out.println("onPluginReconfigured");
    }

    @Override
    public void onMissingPluginDiscovered(String pluginId) {
        System.out.println("onMissingPluginDiscovered");
    }

    @Override
    public void onStartReadPluginConfiguration(EtfsPlugin plugin) {
        System.out.println("onStartReadPluginConfiguration");
    }

    @Override
    public void onEndReadPluginConfiguration(EtfsPlugin plugin) {
        System.out.println("onEndReadPluginConfiguration");
    }

    @Override
    public void onStartReadTableOfContentSection(long numberOfEntries) {
        System.out.println("onStartReadTableOfContentSection");
    }

    @Override
    public void onStartReadTableOfContentsEntry() {
        System.out.println("onStartReadTableOfContentsEntry");
    }

    @Override
    public void onEndReadTableOfContentsEntry(FileEntry entry) {
        System.out.println("onEndReadTableOfContentsEntry");
    }

    @Override
    public void onEndReadTableOfContentSection(TableOfContentSection tableOfContentSection) {
        System.out.println("onEndReadTableOfContentSection");
    }

    @Override
    public void onStartReadFileSection(long entries) {
        System.out.println("onStartReadFileSection");
    }

    @Override
    public void onEndReadFileSection() {
        System.out.println("onEndReadFileSection");
    }

    @Override
    public void onStartReadFile(Path target, long fileSize) {
        System.out.println("onStartReadFile");
    }

    @Override
    public void onEndReadFile(Path target, long fileSize) {
        System.out.println("onEndReadFile");
    }

    @Override
    public void onUnreadableFile() {
        System.out.println("onUnreadableFile");
    }

    @Override
    public void onFileProgress(Path target, long currentPosition, long fileSize) {
        System.out.println("onFileProgress");
    }

    @Override
    public void onStartReadPluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries) {
        System.out.println("onStartReadPluginCustomDataSection");
    }

    @Override
    public void onEndReadPluginCustomDataSection(MetadataPosition metadataPosition) {
        System.out.println("onEndReadPluginCustomDataSection");
    }

    @Override
    public void onStartReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {
        System.out.println("onStartReadPluginCustomData");
    }

    @Override
    public void onEndReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {
        System.out.println("onEndReadPluginCustomData");
    }

    @Override
    public void onReadAbort(ReadAbortReason reason, Exception cause, Object... args) {
        System.out.println("onReadAbort");
    }

    @Override
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e) {
        System.out.println("onUnhandledPluginException");
    }

    @Override
    public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int expectedTapeIndex) {
        System.out.println("multitapeTapeChangeNeeded(volumeLabel="+volumeLabel+", volumeId="+volumeId+", currentTapeIndex="+currentTapeIndex+", expectedTapeIndex="+expectedTapeIndex+")");
        return MultitapeAction.ABORT;
    }

    @Override
    public MultitapeAction multitapeTapeChangeError(EtfsException exception, EtfsException.ErrorCode errorCode) {
        System.out.println("multitapeTapeChangeError(exception="+exception.getMessage()+", errorCode="+errorCode+")");
        exception.printStackTrace();
        return MultitapeAction.ABORT;
    }
}
