package de.notepass.notaping.test.callback;

import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TemporaryFilePathRemapper implements PathRemapper {
    private List<Path> remappedFiles = new ArrayList<>();

    @Override
    public void prepareFileMapping(List<FileEntry> allFiles) {

    }

    @Override
    public Path map(String normalizedPath) {
        Path currentPath = Path.of(System.getProperty("java.io.tmpdir"), "ETFS-TEST-READ-FILE-CONTENT-"+ UUID.randomUUID()+".tmp");
        currentPath.toFile().deleteOnExit();
        remappedFiles.add(currentPath);
        return currentPath;
    }

    public List<Path> getRemappedFiles() {
        return remappedFiles;
    }
}
