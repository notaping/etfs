package de.notepass.notaping.test.callback;

import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import org.junit.Assert;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.UUID;

public class ValidatingWriteProgressCallback implements EtfsWriteProgressCallback {
    int callIndex = 0;

    private void validateAndIncrementIndex(int[] expected, String methodName) {
        Assert.assertTrue(
                "Method \""+methodName+"\" was not called in the expected order. It should have been called at " +
                        "the following points in the call order: "+Arrays.toString(expected)+", but was called at index " +
                        callIndex,
                Arrays.stream(expected).anyMatch(value -> value == callIndex)
        );

        callIndex++;
    }

    public void incrementIndex() {
        callIndex++;
    }

    @Override
    public long getReportFileWriteProgressBytes() {
        return 64;
    }

    @Override
    public long getReportOverallFileWriteProgressBytes() {
        return Long.MAX_VALUE;
    }

    @Override
    public void onStartWriteHeaderSection(HeaderSection headerSection) {
        int[] expectedIndex = {0};
        validateAndIncrementIndex(expectedIndex, "onStartWriteHeaderSection()");
    }

    @Override
    public void onEndWriteHeaderSection(HeaderSection headerSection) {
        int[] expectedIndex = {1};
        validateAndIncrementIndex(expectedIndex, "onEndWriteHeaderSection()");
    }

    @Override
    public void onStartWritePluginConfigSection(PluginConfigSection pluginConfigSection) {
        int[] expectedIndex = {2};
        validateAndIncrementIndex(expectedIndex, "onStartWritePluginConfigSection()");
    }

    @Override
    public void onEndWritePluginConfigSection(PluginConfigSection pluginConfigSection) {
        int[] expectedIndex = {11};
        validateAndIncrementIndex(expectedIndex, "onEndWritePluginConfigSection()");
    }

    @Override
    public void onStartWritePluginInfo(EtfsPlugin plugin) {
        int[] expectedIndex = {3};
        validateAndIncrementIndex(expectedIndex, "onStartWritePluginInfo(plugin="+plugin.getID()+")");
    }

    @Override
    public void onEndWritePluginInfo(EtfsPlugin plugin) {
        int[] expectedIndex = {4};
        validateAndIncrementIndex(expectedIndex, "onEndWritePluginInfo(plugin="+plugin.getID()+")");
    }

    @Override
    public void onStartWritePluginConfiguration(EtfsPlugin plugin) {
        int[] expectedIndex = {5};
        validateAndIncrementIndex(expectedIndex, "onStartWritePluginConfiguration(plugin="+plugin.getID()+")");
    }

    @Override
    public void onEndWritePluginConfiguration(EtfsPlugin plugin) {
        int[] expectedIndex = {6};
        validateAndIncrementIndex(expectedIndex, "onEndWritePluginConfiguration(plugin="+plugin.getID()+")");
    }

    @Override
    public void onStartWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {
        int[] expectedIndex = {12};
        validateAndIncrementIndex(expectedIndex, "onStartWriteTableOfContentSection()");
    }

    @Override
    public void onStartWriteTableOfContentsEntry(FileEntry entry) {
        int[] expectedIndex = {13};
        validateAndIncrementIndex(expectedIndex, "onStartWriteTableOfContentsEntry(file="+entry.getTargetFile()+")");
    }

    @Override
    public void onEndWriteTableOfContentsEntry(FileEntry entry) {
        int[] expectedIndex = {18};
        validateAndIncrementIndex(expectedIndex, "onEndWriteTableOfContentsEntry(file="+entry.getTargetFile()+")");
    }

    @Override
    public void onEndWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {
        int[] expectedIndex = {19};
        validateAndIncrementIndex(expectedIndex, "onEndWriteTableOfContentSection()");
    }

    @Override
    public void onStartWriteFileSection(long entries) {
        int[] expectedIndex = {20};
        validateAndIncrementIndex(expectedIndex, "onStartWriteFileSection()");
    }

    @Override
    public void onEndWriteFileSection() {
        int[] expectedIndex = {28};
        validateAndIncrementIndex(expectedIndex, "onEndWriteFileSection()");
    }

    @Override
    public void onStartWriteFile(Path target, long fileSize) {
        int[] expectedIndex = {21};
        validateAndIncrementIndex(expectedIndex, "onStartWriteFile(file="+target+")");
    }

    @Override
    public void onEndWriteFile(Path target, long fileSize) {
        int[] expectedIndex = {23};
        validateAndIncrementIndex(expectedIndex, "onEndWriteFile(file="+target+")");
    }

    @Override
    public void onUnreadableFile(Path target, IOException e) {
        incrementIndex();
    }

    @Override
    public void onFileProgress(Path target, long currentPosition, long fileSize) {
        int[] expectedIndex = {22};
        validateAndIncrementIndex(expectedIndex, "onFileProgress(file="+target+")");
    }

    @Override
    public void onStartWritePluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries) {
        int[] expectedIndex = {7, 14, 24};
        validateAndIncrementIndex(expectedIndex, "onStartWritePluginCustomDataSection(metadataPosition="+metadataPosition+")");
    }

    @Override
    public void onEndWritePluginCustomDataSection(MetadataPosition metadataPosition) {
        int[] expectedIndex = {10, 17, 27};
        validateAndIncrementIndex(expectedIndex, "onEndWritePluginCustomDataSection(metadataPosition="+metadataPosition+")");
    }

    @Override
    public void onStartWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {
        int[] expectedIndex = {8, 15, 25};
        validateAndIncrementIndex(expectedIndex, "onStartWritePluginCustomData(metadataPosition="+metadataPosition+", plugin="+plugin.getID()+")");
    }

    @Override
    public void onEndWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {
        int[] expectedIndex = {9, 16, 26};
        validateAndIncrementIndex(expectedIndex, "onEndWritePluginCustomData(metadataPosition="+metadataPosition+", plugin="+plugin.getID()+")");
    }

    @Override
    public void onWriteAbort() {
        incrementIndex();
    }

    @Override
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e) {
        incrementIndex();
    }

    @Override
    public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int nextTapeIndex) {
        return MultitapeAction.ABORT;
    }

    @Override
    public void onOverallFileProgress(long bytesWritten, long overallBytesToWrite) {
        incrementIndex();
    }
}
