package de.notepass.notaping.test.callback;

import de.notepass.notaping.core.fs.etfs.EtfsException;
import de.notepass.notaping.core.fs.etfs.data.ReadAbortReason;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.io.IOException;
import java.nio.file.Path;
import java.util.UUID;

public class NoopReadProgressCallback implements EtfsReadProgressCallback {
    @Override
    public long getReportFileWriteProgressBytes() {
        return DEFAULT_REPORT_SIZE;
    }

    @Override
    public void onStartReadHeaderSection() {

    }

    @Override
    public void onEndReadHeaderSection(HeaderSection headerSection) {

    }

    @Override
    public void onStartReadPluginConfigSection(short numEntries) {

    }

    @Override
    public void onEndReadPluginConfigSection(PluginConfigSection pluginConfigSection) {

    }

    @Override
    public void onPluginDiscovered(EtfsPlugin plugin) {

    }

    @Override
    public void onPluginReconfigured(EtfsPlugin plugin) {

    }

    @Override
    public void onMissingPluginDiscovered(String pluginId) {

    }

    @Override
    public void onStartReadPluginConfiguration(EtfsPlugin plugin) {

    }

    @Override
    public void onEndReadPluginConfiguration(EtfsPlugin plugin) {

    }

    @Override
    public void onStartReadTableOfContentSection(long numberOfEntries) {

    }

    @Override
    public void onStartReadTableOfContentsEntry() {

    }

    @Override
    public void onEndReadTableOfContentsEntry(FileEntry entry) {

    }

    @Override
    public void onEndReadTableOfContentSection(TableOfContentSection tableOfContentSection) {

    }

    @Override
    public void onStartReadFileSection(long entries) {

    }

    @Override
    public void onEndReadFileSection() {

    }

    @Override
    public void onStartReadFile(Path target, long fileSize) {

    }

    @Override
    public void onEndReadFile(Path target, long fileSize) {

    }

    @Override
    public void onUnreadableFile() {

    }

    @Override
    public void onFileProgress(Path target, long currentPosition, long fileSize) {

    }

    @Override
    public void onStartReadPluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries) {

    }

    @Override
    public void onEndReadPluginCustomDataSection(MetadataPosition metadataPosition) {

    }

    @Override
    public void onStartReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {

    }

    @Override
    public void onEndReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {

    }

    @Override
    public void onReadAbort(ReadAbortReason reason, Exception cause, Object... args) {

    }

    @Override
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e) {

    }

    @Override
    public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int expectedTapeIndex) throws IOException {
        return MultitapeAction.ABORT;
    }

    @Override
    public MultitapeAction multitapeTapeChangeError(EtfsException exception, EtfsException.ErrorCode errorCode) throws IOException {
        return MultitapeAction.ABORT;
    }
}
