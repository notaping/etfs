package de.notepass.notaping.test.opMapper;

import de.notepass.notaping.core.access.jTapeAccess.impl.mock.FileMockedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.BlockSizeCorrectingTapeOutputStream;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;
import de.notepass.notaping.core.fs.etfs.action.model.TapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.data.metadata.HeaderMetaDataField;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

public class MockImplementaionOpMaper implements TapeSpecificOperationDispatcher {
    private FileMockedTapeDrive tapeDrive;
    private boolean withFilemarkSupport;
    private int tapeCapacity;

    public MockImplementaionOpMaper(boolean withFilemarkSupport, int tapeCapacity) throws TapeException {
        this.tapeCapacity = tapeCapacity;
        this.withFilemarkSupport = withFilemarkSupport;
        tapeDrive = new FileMockedTapeDrive();
    }

    public MockImplementaionOpMaper(boolean withFilemarkSupport, int tapeCapacity, Path tapeFile, FileMockedTapeDrive.WORK_DIRECTION workDirection) throws TapeException {
        this.withFilemarkSupport = withFilemarkSupport;
        this.tapeCapacity = tapeCapacity;
        tapeDrive = new FileMockedTapeDrive();
        tapeDrive.prepare(tapeFile.toString(), withFilemarkSupport, workDirection, tapeCapacity);
        tapeDrive.loadTape();
    }

    public void switchTapeFile(Path tapeFile, FileMockedTapeDrive.WORK_DIRECTION workDirection) throws TapeException {
        tapeDrive.ejectTape();
        tapeDrive.prepare(tapeFile.toString(), withFilemarkSupport, workDirection, tapeCapacity);
        tapeDrive.loadTape();
    }

    public FileMockedTapeDrive getTapeDrive() {
        return tapeDrive;
    }

    @Override
    public HeaderMetaDataField.HEADER_METADATA_FLAG[] getCapabilities() {
        return new HeaderMetaDataField.HEADER_METADATA_FLAG[0];
    }

    public void writeEofMark(OutputStream os, int amount) throws IOException {
        try {
            tapeDrive.writeFilemarks(amount);
        } catch (TapeException e) {
            throw new IOException(e);
        }
    }

    public void advanceEofMarks(InputStream is, int amount) throws IOException {
        tapeDrive.forwardFilemarks(amount);
    }

    @Override
    public boolean isEndOfTapeException(Exception e) {
        return e.getMessage().equals("Virtual tape is at the end");
    }

    @Override
    public InputStream getCurrentInputStream() throws IOException {
        return tapeDrive.getInputStream();
    }

    @Override
    public WrittenBytesAwareOutputStream getCurrentOutputStream() throws IOException {
        return tapeDrive.getOutputStream();
    }

    @Override
    public void writeFilemarks(int amount) throws TapeException {
        tapeDrive.writeFilemarks(amount);
    }

    @Override
    public int setBytePosition(long byteIndex) throws IOException {
        long blockSize = tapeDrive.getTapeInfo().getBlockSizeBytes();
        long blockIndex = (long) Math.floor(((double)byteIndex) / blockSize);
        int blockOffset = (int) (byteIndex - (blockIndex * blockSize));

        tapeDrive.setTapeBlockPosition(blockIndex);

        return blockOffset;
    }

    @Override
    public WrittenBytesAwareOutputStream getTemporaryHeaderOutputStream() throws IOException {
        return new BlockSizeCorrectingTapeOutputStream(tapeDrive.getRawOutputStream(), (int) tapeDrive.getTapeInfo().getBlockSizeBytes());
    }

    @Override
    public void discardTemporaryHeaderOutputStream(WrittenBytesAwareOutputStream os) throws IOException {
        if (tapeDrive.getOutputStream() instanceof BlockSizeCorrectingTapeOutputStream && os instanceof BlockSizeCorrectingTapeOutputStream) {
            ((BlockSizeCorrectingTapeOutputStream) tapeDrive.getOutputStream()).integrateBuffer((BlockSizeCorrectingTapeOutputStream) os, true);
        }
    }
}
