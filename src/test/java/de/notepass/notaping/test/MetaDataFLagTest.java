package de.notepass.notaping.test;

import de.notepass.notaping.core.fs.etfs.data.metadata.FileRuntimeMetaDataField;
import org.junit.Assert;
import org.junit.Test;

public class MetaDataFLagTest {
    @Test
    public void testRunTimeMetaDataWrapper() {
        FileRuntimeMetaDataField runtimeMetaData = new FileRuntimeMetaDataField();
        Assert.assertEquals("Default value for runtimeMetaData should be 0", 0, runtimeMetaData.getRuntimeMetadata());

        runtimeMetaData.setFlag(FileRuntimeMetaDataField.RUNTIME_METADATA_FLAG.FILE_NOT_READABLE);
        Assert.assertEquals("FILE_NOT_READABLE was set wrongly (Should be 0x00000001)", 0x00000001, runtimeMetaData.getRuntimeMetadata());

        runtimeMetaData.setFlag(FileRuntimeMetaDataField.RUNTIME_METADATA_FLAG.FILE_NOT_WRITABLE);
        Assert.assertEquals("FILE_NOT_READABLE was set wrongly (Should be 0x00000003)", 0x00000003, runtimeMetaData.getRuntimeMetadata());

        runtimeMetaData.unsetFlag(FileRuntimeMetaDataField.RUNTIME_METADATA_FLAG.FILE_NOT_READABLE);
        Assert.assertEquals("FILE_NOT_READABLE was unset wrongly (Should be 0x00000002)", 0x00000002, runtimeMetaData.getRuntimeMetadata());

        runtimeMetaData.unsetFlag(FileRuntimeMetaDataField.RUNTIME_METADATA_FLAG.FILE_NOT_WRITABLE);
        Assert.assertEquals("FILE_NOT_READABLE was unset wrongly (Should be 0x00000000)", 0x00000000, runtimeMetaData.getRuntimeMetadata());
    }
}
