package de.notepass.notaping.test;

import de.notepass.notaping.test.callback.NoopReadProgressCallback;
import de.notepass.notaping.test.callback.NoopWriteProgressCallback;
import de.notepass.notaping.test.callback.ValidatingWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsReadDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsWriteDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.shared.SimpleWrittenBytesAwareOutputStreamWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import de.notepass.notaping.test.plugin.TestPlugin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;

/**
 * Tests the simpeleset interactions with ETFS: Write and read one file with one plugin on one tape
 */
public class EtfsSimpleSerializeTest {
    private Path outputTapeFile;
    private Path inputFile;
    private Path remappedFile;
    private EtfsWriteDirectionBuilder writeBuilder;
    private EtfsReadDirectionBuilder readBuilder;
    private byte[] inputFileData;

    @Before
    public void prepareFilesAndInstance() throws IOException {
        inputFileData = new byte[100];
        new Random().nextBytes(inputFileData);
        outputTapeFile = Files.createTempFile("TAPE-OUTPUT-", ".tmp");
        inputFile = Files.createTempFile("TAPE-CONTENT-", ".tmp");
        remappedFile = Files.createTempFile("TAPE-CONTENT-", ".tmp");
        Files.write(inputFile, inputFileData);
        writeBuilder = EtfsWriteDirectionBuilder.newInstance()
                .addFile(inputFile)
                .withLabel("TEST_TAPE");
        readBuilder = EtfsReadDirectionBuilder.newInstance(new PathRemapper() {
            @Override
            public void prepareFileMapping(List<FileEntry> allFiles) {

            }

            @Override
            public Path map(String normalizedPath) {
                return remappedFile;
            }
        });
    }

    /**
     * Tests that a simple filesystem could be written
     * @throws IOException
     */
    public void testBasicSerialize() throws IOException {
        try (OutputStream outputFileStream = Files.newOutputStream(outputTapeFile)) {
            writeBuilder.
                    addPlugin(new TestPlugin())
                    .build()
                    .serialize(new SimpleWrittenBytesAwareOutputStreamWrapper(outputFileStream), new NoopWriteProgressCallback());
        }
    }

    /**
     * Test to check, that the de.notepass.notaping.test.callback is called in the correct order on a normal run
     */
    @Test
    public void testCallbackIntegrity() throws IOException {
        try (OutputStream outputFileStream = Files.newOutputStream(outputTapeFile)) {
            System.setProperty("etfs.core.buffer.read.size", "64");
            writeBuilder.
                    addPlugin(new TestPlugin())
                    .build()
                    .serialize(new SimpleWrittenBytesAwareOutputStreamWrapper(outputFileStream), new ValidatingWriteProgressCallback());
            System.clearProperty("etfs.core.buffer.read.size");
        }
    }

    @Test
    public void testDeserialize() throws IOException {
        testBasicSerialize();
        readBuilder.addPlugin(new TestPlugin());
        EtfsInstance i = readBuilder.build();
        try (InputStream is = Files.newInputStream(outputTapeFile)) {
            i.deserialize(is, new NoopReadProgressCallback(), false);
        }
        Assert.assertArrayEquals("Data written to disk does not equal data read from disk", Files.readAllBytes(remappedFile), Files.readAllBytes(inputFile));
    }
}
