package de.notepass.notaping.core.fs.etfs.data.metadata;

public class FileMetaDataField {
    public enum FILE_METADATA_FLAG {
        //TODO: [Blocker] Document flag
        /**
         * State indicating that only part of this file is on this tape, the other part is on a previous multitape
         */
        IS_MULTITAPE_SPLITTED(0x000000000001L),

        /**
         * Indicates that this is the first entry on the current tape
         */
        IS_FIRST_ENTRY(0x0000000000010L),

        /**
         * State indicating that the file was not written to the tape because of an error
         */
        IS_NOT_WRITTEN(0x100000000000L);

        private long mask;

        FILE_METADATA_FLAG(long mask) {
            this.mask = mask;
        }

        public long getSetMask() {
            return mask;
        }

        public long getUnsetMask() {
            return mask ^ 0xFFFFFFFFFFFFL;
        }
    }

    private long fileMetadata;

    public FileMetaDataField() {
    }

    public FileMetaDataField(long fileMetadata) {
        this.fileMetadata = fileMetadata;
    }

    public void setFlag(FILE_METADATA_FLAG flag) {
        fileMetadata |= flag.getSetMask();
    }

    public boolean getFlag(FILE_METADATA_FLAG flag) {
        return (fileMetadata & flag.getSetMask()) != 0;
    }

    public void unsetFlag(FILE_METADATA_FLAG flag) {
        fileMetadata &= flag.getUnsetMask();
    }

    public long getFileMetadata() {
        return fileMetadata;
    }

    public void setFileMetadata(long runtimeMetadata) {
        this.fileMetadata = runtimeMetadata;
    }
}
