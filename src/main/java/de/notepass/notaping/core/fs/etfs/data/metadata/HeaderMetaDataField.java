package de.notepass.notaping.core.fs.etfs.data.metadata;

public class HeaderMetaDataField {
    public enum HEADER_METADATA_FLAG {
        ;

        private long mask;

        HEADER_METADATA_FLAG(long mask) {
            this.mask = mask;
        }

        public long getSetMask() {
            return mask;
        }

        public long getUnsetMask() {
            return mask ^ 0xFFFFFFFFFFFFL;
        }
    }

    private long headerMetadata;

    public HeaderMetaDataField() {
    }

    public HeaderMetaDataField(long fileMetadata) {
        this.headerMetadata = fileMetadata;
    }

    public void setFlag(HEADER_METADATA_FLAG flag) {
        headerMetadata |= flag.getSetMask();
    }

    public boolean getFlag(HEADER_METADATA_FLAG flag) {
        return (headerMetadata & flag.getSetMask()) != 0;
    }

    public void unsetFlag(HEADER_METADATA_FLAG flag) {
        headerMetadata &= flag.getUnsetMask();
    }

    public long getHeaderMetadata() {
        return headerMetadata;
    }

    public void setHeaderMetadata(long runtimeMetadata) {
        this.headerMetadata = runtimeMetadata;
    }
}
