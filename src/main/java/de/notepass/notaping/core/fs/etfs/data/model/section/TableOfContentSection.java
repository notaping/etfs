package de.notepass.notaping.core.fs.etfs.data.model.section;

import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Java class representation of the "Table of content" section, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class TableOfContentSection implements EtfsSerializableElement {
    // file_entries_length: 8 int: Number of entries in the file_entries list
    private long fileEntriesLength;

    // file_entries: 0:9223372036854775807 list<FileEntry>: List of file metadata present
    private List<FileEntry> fileEntries = new ArrayList<>();

    /**
     * file_entries_length: 8 int: Number of entries in the file_entries list
     * @return
     */
    public long getFileEntriesLength() {
        return fileEntriesLength;
    }

    /**
     * file_entries_length: 8 int: Number of entries in the file_entries list
     * @param fileEntriesLength
     */
    public void setFileEntriesLength(long fileEntriesLength) {
        this.fileEntriesLength = fileEntriesLength;
    }

    /**
     * file_entries: 0:9223372036854775807 list<FileEntry>: List of file metadata present
     * @return
     */
    public List<FileEntry> getFileEntries() {
        return fileEntries;
    }

    /**
     * file_entries: 0:9223372036854775807 list<FileEntry>: List of file metadata present
     * @param fileEntries
     */
    public void setFileEntries(List<FileEntry> fileEntries) {
        this.fileEntries = fileEntries;
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        fileEntriesLength = getFileEntries().size();

        callback.onStartWriteTableOfContentSection(this);
        writeAndCheck(toBytes(fileEntriesLength), os);
        for (FileEntry fe : getFileEntries()) {
            os = fe.serialize(os, isMultitapePart, context, callback);
        }
        callback.onEndWriteTableOfContentSection(this);

        return os;
    }

    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        fileEntriesLength = readNextLong(is);
        callback.onStartReadTableOfContentSection(fileEntriesLength);
        for (long i = 0; i < fileEntriesLength; i++) {
            FileEntry fe = new FileEntry();
            fileEntries.add(fe);
            is = fe.deserialize(is, version, ignorePluginSections, context, callback);
        }
        callback.onEndReadTableOfContentSection(this);

        context.getFileSelector().prepareFileSelection(fileEntries);
        context.getPathRemapper().prepareFileMapping(fileEntries);
        for (FileEntry entry : fileEntries) {
            entry.setRestore(context.getFileSelector().writeFile(entry));
            entry.setTargetFile(context.getPathRemapper().map(entry.getFilename()));
        }

        return is;
    }
}
