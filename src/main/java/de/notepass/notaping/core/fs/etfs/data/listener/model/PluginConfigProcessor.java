package de.notepass.notaping.core.fs.etfs.data.listener.model;

import java.io.InputStream;
import java.io.OutputStream;

public interface PluginConfigProcessor {
    /**
     * Called when data is read from the tape
     * @param is InputStream of the tape
     */
    public void onPluginConfigStart(InputStream is);

    /**
     * Called when data should be written to the tape
     * @param os OutputStream of the tape
     */
    public void onPluginConfigStart(OutputStream os);
}
