package de.notepass.notaping.core.fs.etfs;

import java.io.IOException;

public class EtfsException extends IOException {
    public static enum ErrorCode {
        /**
         * Indicates that the tape inserted does not belong to the same multitape volume and is thus being rejected
         */
        MULTITAPE_DIFFERENT_VOLUME,

        /**
         * Indicates that the inserted tape belongs to the same volume, but is not the next tape in the order
         */
        MULTITAPE_NOT_NEXT_TAPE,

        /**
         * Unspecified error (See cause)
         */
        MISC_ERROR,

        /**
         * Indicates that the operation has been aborted by the user
         */
        USER_ABORT
    }

    private final ErrorCode errorCode;

    public EtfsException(String message, ErrorCode code) {
        super(message);
        this.errorCode = code;
    }

    public EtfsException(String message, Throwable cause, ErrorCode code) {
        super(message, cause);
        this.errorCode = code;
    }

    public EtfsException(Throwable cause, ErrorCode code) {
        super(cause);
        this.errorCode = code;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
