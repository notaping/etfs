package de.notepass.notaping.core.fs.etfs.data.listener.model;

import de.notepass.notaping.core.fs.etfs.EtfsException;
import de.notepass.notaping.core.fs.etfs.data.ReadAbortReason;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.io.IOException;
import java.nio.file.Path;
import java.util.UUID;

public interface EtfsReadProgressCallback {
    public static final long DEFAULT_REPORT_SIZE = 64*1024;

    /**
     * The amount of bytes written after which {@link #onFileProgress(Path, long, long)} is triggered
     */
    public long getReportFileWriteProgressBytes();

    // ================== Header section ==================

    /**
     * This method is called before parsing the header section
     */
    public void onStartReadHeaderSection();

    /**
     * This method is called after the header section was read an parsed
     * @param headerSection HeaderSection which was read
     */
    public void onEndReadHeaderSection(HeaderSection headerSection);

    // ================== Plugin config section ==================

    /**
     * This method is called before the "Plugin config section" is read
     * @param numEntries The number of entries in the config section
     */
    public void onStartReadPluginConfigSection(short numEntries);

    /**
     * This method is called after the "Plugin config section" is read and parsed
     * @param pluginConfigSection Parsed pluginconfig section data
     */
    public void onEndReadPluginConfigSection(PluginConfigSection pluginConfigSection);

    /**
     * This method is called, if an existing plugin was found on the tape and parsed
     * @param plugin Parsed plugin data
     */
    public void onPluginDiscovered(EtfsPlugin plugin);

    /**
     * This method is called for every plugin that has a configuration set saved on the tape. The method is called,
     * after the plugin has parsed its data
     * @param plugin Plugin which was reconfigured
     */
    public void onPluginReconfigured(EtfsPlugin plugin);

    /**
     * This method is called, if an plugin was found on the tape which isn't available locally
     * @param pluginId ID of the plugin saved on the tape
     */
    public void onMissingPluginDiscovered(String pluginId);

    /**
     * This method is called before the custom config of a plugin is written to the tape
     * @param plugin Plugin writing the data
     */
    public void onStartReadPluginConfiguration(EtfsPlugin plugin);

    /**
     * This method is called after the custom config of a plugin is written to the tape
     * @param plugin Plugin writing the data
     */
    public void onEndReadPluginConfiguration(EtfsPlugin plugin);

    // ================== TOC section ==================

    /**
     * This method is called before the table of contents section is read and parsed
     * @param numberOfEntries The number of entries in the TOC section
     */
    public void onStartReadTableOfContentSection(long numberOfEntries);

    /**
     * This method is called before a file entry is read from the TOC
     */
    public void onStartReadTableOfContentsEntry();

    /**
     * This method is called after a file entry was read from the TOC
     * @param entry The parsed file entry
     */
    public void onEndReadTableOfContentsEntry(FileEntry entry);

    /**
     * This method is called after the table of contents section is parsed
     * @param tableOfContentSection The parsed section data
     */
    public void onEndReadTableOfContentSection(TableOfContentSection tableOfContentSection);

    // ================== File section ==================

    /**
     * This method is called before the file section is read
     * @param entries Number of files in the file section. Please note, that they don't have to be on the same tape,
     *                as multitape volumes exist
     */
    public void onStartReadFileSection(long entries);

    /**
     * This method is called after the file section is read
     */
    public void onEndReadFileSection();

    /**
     * This method is called before processing a file
     * @param target Reference to the file target
     * @param fileSize Size of the file
     */
    public void onStartReadFile(Path target, long fileSize);

    /**
     * This method is called after processing a file
     * @param target Reference to the file target
     * @param bytesRead Amount of bytes read
     */
    public void onEndReadFile(Path target, long bytesRead);

    /**
     * This method is called if a file on the tape cannot be processed (e.g. because it couldn't be written to the tape)
     */
    public void onUnreadableFile();

    /**
     * This method is called while processing files. It is called after {@link #reportByteSize} bytes where read from the tape
     * @param target Reference to the file target
     * @param currentPosition Amount of bytes already read
     * @param fileSize Overall size of the file
     */
    public void onFileProgress(Path target, long currentPosition, long fileSize);

    // ================== Plugin custom data section ==================

    /**
     * This method is called before the plugin custom data block (=list) at a specific position is read
     * @param metadataPosition Which position the metadata is at
     * @param totalEntries The total amount of entries in the list
     */
    public void onStartReadPluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries);

    /**
     * This method is called after the plugin custom data block (=list) at a specific position is read
     * @param metadataPosition Which position the metadata is at
     */
    public void onEndReadPluginCustomDataSection(MetadataPosition metadataPosition);

    /**
     * This method is called before a single plugin custom data entry is read
     * @param metadataPosition Which position the metadata is at
     * @param plugin The plugin which will read the data
     */
    public void onStartReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin);

    /**
     * This method is called after a single plugin custom data entry is read
     * @param metadataPosition Which position the metadata is at
     * @param plugin The plugin which will read the data
     */
    public void onEndReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin);

    // ================== MISC section ==================

    /**
     * This method is called if reading the filesystem is aborted for some reason (e.g. incompatible version or unrecoverable
     * error)
     * @param args Additional, error-specific, arguments. Normally given in order expectedValue, actualValue. The following data might be given based on
     *             the abort reason:
     *             <ul>
     *             <li>WRONG_TAPE_FORMAT</li>
     *             <ol>
     *             <li>Param: String: Expected magic string</li>
     *             <li>Param: String: Actual magic string</li>
     *             </ol>
     *             <li>INCOMPATIBLE_VERSION</li>
     *             <ol>
     *             <li>Param: int: Expected minimum version</li>
     *             <li>Param: int: Actual version</li>
     *             </ol>
     *             </ul>
     */
    public void onReadAbort(ReadAbortReason reason, Exception cause, Object ... args);

    /**
     * This method will be called, if a plugin threw an unhandled exception. The exception will be caught by ETFS core
     * and the custom data entry will be skipped
     * @param plugin The offending plugin
     * @param e The exception thrown
     */
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e);


    /**
     * This method is called when the end of a tape in a multitape volume is reached. It should block until the next tape
     * is ready to be read
     * @param volumeLabel The user defined label of the
     * @param volumeId The autotomatically generated ID of the multiutape volume
     * @param currentTapeIndex The current tape index (Number of the, satrting with 0)
     * @param expectedTapeIndex The index of the next tape expected (normally currentTapeIndex + 1)
     * @return If reading should continue (TAPE_CHANGED) or be aborted (CANCEL)
     */
    public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int expectedTapeIndex) throws IOException;

    /**
     * Indicates that the tape changed in via {@link #multitapeTapeChangeNeeded(String, UUID, int, int)} is not the expected
     * tape. This method should block until the tape drive is in a valid state for the given return value
     * @param exception The complete exception encountered
     * @param errorCode The error code encountered for easier checking
     * @return If the current tape should still be used if possible (CONTINUE), the tape was changed and the new tape
     * should be evaluated (TAPE_CHANGED) or the operation should be canceled
     */
    public MultitapeAction multitapeTapeChangeError(EtfsException exception, EtfsException.ErrorCode errorCode) throws IOException;
}
