package de.notepass.notaping.core.fs.etfs.data.runtime;

import de.notepass.notaping.core.fs.etfs.EtfsException;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.action.model.TapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.data.config.EtfsCoreConfiguration;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.action.model.FileSelector;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.metadata.FileMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.PluginRegistry;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public class EtfsInstance {
    private LogicalTapeInfo logicalTapeInfo = new LogicalTapeInfo();
    private List<PhysicalTapeInfo> physicalTapeInfo = new LinkedList<>();


    // Plugins which should be used when writing or were detected when reading
    private PluginRegistry pluginRegistry;

    // Maps normalized paths to paths on the target file system for writing
    private PathRemapper pathRemapper;

    // To keep the data structure separated from the JTapeAccess API, this glue dispatcher will run tape specific operations
    // This also means, that the data structure has to support streams with and without EOF mark support (e.g. file stream and tape stream)
    private TapeSpecificOperationDispatcher opDispatcher;

    // Selects which files to restore
    private FileSelector fileSelector;

    // Current tape in multitape volume
    private short tapeIndex;

    private static final Logger LOGGER = LoggerFactory.getLogger(EtfsInstance.class);

    protected EtfsInstance() {
        physicalTapeInfo.add(new PhysicalTapeInfo());
    }

    /**
     * Return the info of the current (or last if none inserted) tape
     * @return
     */
    public PhysicalTapeInfo getCurrentPhysicalTapeInfo() {
        return physicalTapeInfo.get(physicalTapeInfo.size() - 1);
    }

    /**
     * Returns the plugin registry for this ETFS instance
     * @return
     */
    public PluginRegistry getPluginRegistry() {
        return pluginRegistry;
    }

    /**
     * Sets the plugin manager for this ETFS instance
     * @param pluginRegistry
     */
    public void setPluginRegistry(PluginRegistry pluginRegistry) {
        this.pluginRegistry = pluginRegistry;
    }

    /**
     * Current index in multitape volume, starts at 0
     * @return
     */
    public short getTapeIndex() {
        return tapeIndex;
    }

    /**
     * Maps normalized paths to paths on the target file system for writing
     * @return
     */
    public PathRemapper getPathRemapper() {
        return pathRemapper;
    }

    /**
     * Maps normalized paths to paths on the target file system for writing
     * @param pathRemapper
     */
    public void setPathRemapper(PathRemapper pathRemapper) {
        this.pathRemapper = pathRemapper;
    }

    /**
     * Current index in multitape volume, starts at 0
     * @param tapeIndex
     */
    public void setTapeIndex(short tapeIndex) {
        this.tapeIndex = tapeIndex;
    }

    /**
     * To keep the data structure separated from the JTapeAccess API, this glue dispatcher will run tape specific operations
     * This also means, that the data structure has to support streams with and without EOF mark support (e.g. file stream and tape stream)
     * @return
     */
    public TapeSpecificOperationDispatcher getOpDispatcher() {
        return opDispatcher;
    }

    /**
     * To keep the data structure separated from the JTapeAccess API, this glue dispatcher will run tape specific operations
     * This also means, that the data structure has to support streams with and without EOF mark support (e.g. file stream and tape stream)
     * @param opDispatcher
     */
    public void setOpDispatcher(TapeSpecificOperationDispatcher opDispatcher) {
        this.opDispatcher = opDispatcher;
    }

    /**
     * Selects which files to restore
     * @return
     */
    public FileSelector getFileSelector() {
        return fileSelector;
    }

    /**
     * Selects which files to restore
     * @param fileSelector
     */
    public void setFileSelector(FileSelector fileSelector) {
        this.fileSelector = fileSelector;
    }

    /**
     * Returns data shared across different tapes
     * @return
     */
    public LogicalTapeInfo getLogicalTapeInfo() {
        return logicalTapeInfo;
    }

    /**
     * Returns a list of information only necessary for specific physical tapes in a multitape volume
     * @return
     */
    public List<PhysicalTapeInfo> getPhysicalTapeInfo() {
        return physicalTapeInfo;
    }

    public OutputStream serialize(WrittenBytesAwareOutputStream os, EtfsWriteProgressCallback callback) throws IOException {
        os = getLogicalTapeInfo().getHeaderSection().serialize(os, false, this, callback);
        os = getLogicalTapeInfo().getPluginConfigSection().serialize(os, false, this, callback);
        getLogicalTapeInfo().getTableOfContentSection().getFileEntries().get(0).getFileMeta().setFlag(FileMetaDataField.FILE_METADATA_FLAG.IS_FIRST_ENTRY);
        os = getLogicalTapeInfo().getTableOfContentSection().serialize(os, false, this, callback);
        os = getLogicalTapeInfo().getFileSection().serialize(os, false, this, callback);
        SerializeUtils.flushAndCheckAndRequestTapeChange(os, callback, this, getLogicalTapeInfo().getTableOfContentSection().getFileEntries().get(getLogicalTapeInfo().getTableOfContentSection().getFileEntries().size() - 1));
        opDispatcher.writeFilemarks(1);
        return os;
    }

    public InputStream deserialize(InputStream is, EtfsReadProgressCallback callback) throws IOException {
        return  deserialize(is, callback, false);
    }

    public InputStream deserialize(InputStream is, EtfsReadProgressCallback callback, boolean ignorePluginSections) throws IOException {
        is = getLogicalTapeInfo().getHeaderSection().deserialize(is, -1, ignorePluginSections, this, callback);

        int version = getLogicalTapeInfo().getHeaderSection().getVersion();
        is = getLogicalTapeInfo().getPluginConfigSection().deserialize(is, version, ignorePluginSections, this, callback);
        is = getLogicalTapeInfo().getTableOfContentSection().deserialize(is, version, ignorePluginSections, this, callback);
        is = getLogicalTapeInfo().getFileSection().deserialize(is, version, ignorePluginSections, this, callback);
        return is;
    }

    /**
     * Requests the user to switch the tape and returns the new input stream, if the user switched to the correct one
     * @param is Current input stream
     * @param callback Callback for asking for a new tape
     * @return New InputStream after tape switch
     * @throws IOException If tape not switched/incorrectly switched
     */
    public InputStream switchMutitapeTape(InputStream is, EtfsReadProgressCallback callback) throws IOException {
        MultitapeAction action = callback.multitapeTapeChangeNeeded(
                getLogicalTapeInfo().getHeaderSection().getLabel(),
                getLogicalTapeInfo().getHeaderSection().getVolumeId(),
                getLogicalTapeInfo().getHeaderSection().getMultitapeIndex(),
                tapeIndex + 1
        );

        if (action == MultitapeAction.ABORT) {
            throw new EtfsException("Reading aborted by user when requesting next tape in multitape volume", EtfsException.ErrorCode.USER_ABORT);
        }

        EtfsException processingError = null;

        if (action == MultitapeAction.TAPE_CHANGED) {
            physicalTapeInfo.add(new PhysicalTapeInfo());

            is = opDispatcher.getCurrentInputStream();
            getCurrentPhysicalTapeInfo().setHeaderSection(new HeaderSection());
            getCurrentPhysicalTapeInfo().getHeaderSection().deserialize(is, -1, true, this, callback);

            if (!getCurrentPhysicalTapeInfo().getHeaderSection().getVolumeId().equals(getLogicalTapeInfo().getHeaderSection().getVolumeId())) {
                // Tape of another multitape instance inserted
                processingError = new EtfsException("Inserted tape of another multitape volume. " +
                        "Current volume: ID="+getLogicalTapeInfo().getHeaderSection().getVolumeId()+"; Label="+getLogicalTapeInfo().getHeaderSection().getLabel()+", " +
                        "inserted tape data: ID="+getCurrentPhysicalTapeInfo().getHeaderSection().getVolumeId()+"; Label="+getCurrentPhysicalTapeInfo().getHeaderSection().getLabel(),
                        EtfsException.ErrorCode.MULTITAPE_DIFFERENT_VOLUME);
                action = callback.multitapeTapeChangeError(processingError, processingError.getErrorCode());

                switch (action) {
                    case CONTINUE:
                        // Cannot continue, aborting with internal error
                        throw new EtfsException("Cannot continue reading data from a tape of a different volume as requested. Failing.", processingError, EtfsException.ErrorCode.MISC_ERROR);
                    case ABORT:
                        throw new EtfsException("Reading aborted by user when requesting next tape in multitape volume", processingError, EtfsException.ErrorCode.USER_ABORT);
                    case TAPE_CHANGED:
                        switchMutitapeTape(is, callback);
                        return getOpDispatcher().getCurrentInputStream();
                    default:
                        throw new EtfsException("Invalid response for switch multitape request: "+action, processingError, EtfsException.ErrorCode.MISC_ERROR);
                }
            }

            if (getCurrentPhysicalTapeInfo().getHeaderSection().getMultitapeIndex() != tapeIndex + 1) {
                processingError = new EtfsException("The inserted tape is not the next tape in the multitape volume order." +
                        "Last tape index: "+getTapeIndex()+". Expected index for the new tape: "
                        +(getTapeIndex() + 1)+". Current tape index: "+getCurrentPhysicalTapeInfo().getHeaderSection().getMultitapeIndex(),
                        EtfsException.ErrorCode.MULTITAPE_NOT_NEXT_TAPE);

                action = callback.multitapeTapeChangeError(processingError, processingError.getErrorCode());

                switch (action) {
                    case TAPE_CHANGED:
                        // Remove entry created before, as it was an error and has been changed
                        physicalTapeInfo.remove(physicalTapeInfo.size() - 1);
                        switchMutitapeTape(is, callback);
                        return getOpDispatcher().getCurrentInputStream();
                    case ABORT:
                        throw new EtfsException("Reading aborted by user when requesting next tape in multitape volume", processingError, EtfsException.ErrorCode.USER_ABORT);
                    case CONTINUE:
                        // Do nothing, read data, throw error later, let the file reader do its stuff
                        break;
                }
            }

            tapeIndex = getCurrentPhysicalTapeInfo().getHeaderSection().getMultitapeIndex();
            // Ignore plugin config section as it should be the same on all tapes
            new PluginConfigSection().deserialize(is, EtfsCoreConfiguration.BINARY_VERSION, true, this, callback);

            getCurrentPhysicalTapeInfo().setTableOfContentSection(new TableOfContentSection());
            getCurrentPhysicalTapeInfo().getTableOfContentSection().deserialize(is, EtfsCoreConfiguration.BINARY_VERSION, true, this, callback);

            if (processingError != null) {
                throw processingError;
            }

            return getOpDispatcher().getCurrentInputStream();
        } else {
            throw new EtfsException("Invalid response for switch multitape request: " + action, EtfsException.ErrorCode.MISC_ERROR);
        }
    }

    public void writeNextTapeHeader(FileEntry currentlyWrittenFileEntry, EtfsWriteProgressCallback callback) throws IOException {
        //TODO: [High] Use proper logical/physical tape info structure
        WrittenBytesAwareOutputStream os = opDispatcher.getTemporaryHeaderOutputStream();
        getLogicalTapeInfo().getHeaderSection().setMultitapeIndex((short) (getLogicalTapeInfo().getHeaderSection().getMultitapeIndex() + 1));
        tapeIndex = getLogicalTapeInfo().getHeaderSection().getMultitapeIndex();
        currentlyWrittenFileEntry.getFileMeta().setFlag(FileMetaDataField.FILE_METADATA_FLAG.IS_MULTITAPE_SPLITTED);

        //TODO: [Blocker] Set overall_file_content_entry_site to size of entry remaining on the new tape as stated in documentation

        // Strip existing "is first entry" marks
        getLogicalTapeInfo().getTableOfContentSection()
                .getFileEntries()
                .stream()
                .filter(e -> e.getFileMeta().getFlag(FileMetaDataField.FILE_METADATA_FLAG.IS_FIRST_ENTRY))
                .forEach(e -> e.getFileMeta().unsetFlag(FileMetaDataField.FILE_METADATA_FLAG.IS_FIRST_ENTRY));

        currentlyWrittenFileEntry.getFileMeta().setFlag(FileMetaDataField.FILE_METADATA_FLAG.IS_FIRST_ENTRY);

        getLogicalTapeInfo().getHeaderSection().serialize(os, true, this, callback);
        getLogicalTapeInfo().getPluginConfigSection().serialize(os, true, this, callback);
        getLogicalTapeInfo().getTableOfContentSection().serialize(os, true, this, callback);
        opDispatcher.discardTemporaryHeaderOutputStream(os);
    }
}
