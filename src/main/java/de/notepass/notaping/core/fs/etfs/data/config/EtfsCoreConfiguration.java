package de.notepass.notaping.core.fs.etfs.data.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class EtfsCoreConfiguration {
    /**
     * The version of the library as defined by the build system. This is the version normally referenced<br/>
     * This version is changed upon
     */
    public static final String LIBRARY_VERSION;

    /**
     * Commit ID this library was build from
     */
    public static final String GIT_COMMIT_ID;

    /**
     * Version written to tape (binary version)<br/>
     * This version will only be changed if the physical layout of ETFS changes
     */
    public static final int BINARY_VERSION;

    /**
     * Minimum binary client version needed to read the tape
     */
    public static final int MIN_BINARY_COMPAT_VERSION;

    private static final Logger LOGGER = LoggerFactory.getLogger(EtfsCoreConfiguration.class);

    static {
        Properties coreInfoProps = new Properties();

        try {
            coreInfoProps.load(EtfsCoreConfiguration.class.getResourceAsStream("maven.properties"));
        } catch (Exception e) {
            // TODO: [Low] How to get the properties to work in test cycle?
            LOGGER.error("Could not read ETFS version properties (classpath::/maven.properties). Falling back to nonsense (This is OK for automated tests).", e);
        }

        try {
            coreInfoProps.load(EtfsCoreConfiguration.class.getResourceAsStream("git.properties"));
        } catch (Exception e) {
            // Optional properties. Do not fail
            LOGGER.warn("Cannot access git infos (classpath::/git.properties). Will replace git info with empty string(s)");
        }

        LIBRARY_VERSION = coreInfoProps.getProperty("etfs.library.version", "0.0.0");
        BINARY_VERSION = Integer.parseInt(coreInfoProps.getProperty("etfs.binary.version", "0"));
        MIN_BINARY_COMPAT_VERSION = Integer.parseInt(coreInfoProps.getProperty("etfs.binary.mincompat.version", "0"));
        GIT_COMMIT_ID = coreInfoProps.getProperty("git.commit.id", "");
    }
}
