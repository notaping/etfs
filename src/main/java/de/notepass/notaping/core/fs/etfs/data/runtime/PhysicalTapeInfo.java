package de.notepass.notaping.core.fs.etfs.data.runtime;

import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;

public class PhysicalTapeInfo {
    // Multitape: Tape specific sections
    // Header of the tape currently inserted. May differ from the main header section if a multitape volume is used
    private HeaderSection headerSection;
    private TableOfContentSection tableOfContentSection;

    // Current tape in multitape volume
    private short tapeIndex;

    // Number of bytes written in the file section until now
    private long amountOfFileBytesWritten;
    private long lastReportedFileBytesWritten;

    /**
     * Current index in multitape volume, starts at 0
     * @return
     */
    public short getTapeIndex() {
        return tapeIndex;
    }

    /**
     * Current index in multitape volume, starts at 0
     * @param tapeIndex
     */
    public void setTapeIndex(short tapeIndex) {
        this.tapeIndex = tapeIndex;
    }

    public long getAmountOfFileBytesWritten() {
        return amountOfFileBytesWritten;
    }

    public void setAmountOfFileBytesWritten(long amountOfFileBytesWritten) {
        this.amountOfFileBytesWritten = amountOfFileBytesWritten;
    }

    public long getLastReportedFileBytesWritten() {
        return lastReportedFileBytesWritten;
    }

    public void setLastReportedFileBytesWritten(long lastReportedFileBytesWritten) {
        this.lastReportedFileBytesWritten = lastReportedFileBytesWritten;
    }

    public HeaderSection getHeaderSection() {
        return headerSection;
    }

    public TableOfContentSection getTableOfContentSection() {
        return tableOfContentSection;
    }

    public void setHeaderSection(HeaderSection headerSection) {
        this.headerSection = headerSection;
    }

    public void setTableOfContentSection(TableOfContentSection tableOfContentSection) {
        this.tableOfContentSection = tableOfContentSection;
    }
}
