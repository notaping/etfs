package de.notepass.notaping.core.fs.etfs.action.impl;

import de.notepass.notaping.core.fs.etfs.action.model.FileSelector;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;

import java.util.List;

public class AcceptAllFileSelector implements FileSelector {
    @Override
    public void prepareFileSelection(List<FileEntry> allFiles) {

    }

    @Override
    public boolean writeFile(FileEntry file) {
        return true;
    }
}
