package de.notepass.notaping.core.fs.etfs.action.impl;

import de.notepass.notaping.core.fs.etfs.data.metadata.HeaderMetaDataField;
import de.notepass.notaping.core.fs.etfs.action.model.TapeSpecificOperationDispatcher;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class NoopTapeSpecificOperationDispatcher implements TapeSpecificOperationDispatcher {
    @Override
    public HeaderMetaDataField.HEADER_METADATA_FLAG[] getCapabilities() {
        return new HeaderMetaDataField.HEADER_METADATA_FLAG[0];
    }

    @Override
    public boolean isEndOfTapeException(Exception e) {
        return false;
    }

    @Override
    public InputStream getCurrentInputStream() throws IOException {
        return null;
    }

    @Override
    public WrittenBytesAwareOutputStream getCurrentOutputStream() throws IOException {
        return null;
    }

    @Override
    public void writeFilemarks(int amount) {

    }

    @Override
    public int setBytePosition(long byteIndex) throws IOException {
        return 0;
    }

    @Override
    public WrittenBytesAwareOutputStream getTemporaryHeaderOutputStream() throws IOException {
        return null;
    }

    @Override
    public void discardTemporaryHeaderOutputStream(WrittenBytesAwareOutputStream os) throws IOException {

    }
}
