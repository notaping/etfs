package de.notepass.notaping.core.fs.etfs.data.model.section;

import de.notepass.notaping.core.fs.etfs.data.config.EtfsCoreConfiguration;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.metadata.HeaderMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.ReadAbortReason;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * Java class representation of the "Header" section, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class HeaderSection implements EtfsSerializableElement {
    // magic: 8 string: "Magic string" to indicate ETFS tape. Is always "//ETFS//"
    public static final String MAGIC = "//ETFS//";

    // version: 4 int: Version of the ETFS core used to create the tape
    private int version;

    // min_version: 4 int: Minimum ETFS core version which is necessary to read this tape
    private int minVersion;

    // allow_multitape: 1 int: Boolean value, indicating if this tape can be seen as a possible multitape volume (See WIKI)
    private boolean allowMultitape;

    // multitape_index: 2 int: The index of this tape in a multitape volume. Starts at 0 for the first tape.
    private short multitapeIndex;

    // header_metadata: 8 int: Additional metadata flags, giving information on how the tape was written (See metadata section for more info)
    private HeaderMetaDataField headerMetadata = new HeaderMetaDataField();

    // volume_id_size: 1 int: Size of the tape volume_id in bytes
    private byte volumeIdSize;

    // volume_id: 0:126 string: ID of the volume, for use with multitape volumes
    private UUID volumeId;

    // label_size: 2 int: Size of the tape label in bytes
    private short labelSize;

    // label: 0:32767 string: A user-defined, human readable, label for the tape (Or some other stuff, as long as it can be stringed. I'm a doc,not a cop)
    private String label;

    /**
     * version: 4 int: Version of the ETFS core used to create the tape
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     * version: 4 int: Version of the ETFS core used to create the tape
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * min_version: 4 int: Minimum ETFS core version which is necessary to read this tape
     * @return
     */
    public int getMinVersion() {
        return minVersion;
    }

    /**
     * min_version: 4 int: Minimum ETFS core version which is necessary to read this tape
     * @param minVersion
     */
    public void setMinVersion(int minVersion) {
        this.minVersion = minVersion;
    }

    /**
     * allow_multitape: 1 int: Boolean value, indicating if this tape can be seen as a possible multitape volume (See WIKI)
     * @return
     */
    public boolean isAllowMultitape() {
        return allowMultitape;
    }

    /**
     * allow_multitape: 1 int: Boolean value, indicating if this tape can be seen as a possible multitape volume (See WIKI)
     * @param allowMultitape
     */
    public void setAllowMultitape(boolean allowMultitape) {
        this.allowMultitape = allowMultitape;
    }

    /**
     * multitape_index: 2 int: The index of this tape in a multitape volume. Starts at 0 for the first tape.
     * @return
     */
    public short getMultitapeIndex() {
        return multitapeIndex;
    }

    /**
     * multitape_index: 2 int: The index of this tape in a multitape volume. Starts at 0 for the first tape.
     * @param multitapeIndex
     */
    public void setMultitapeIndex(short multitapeIndex) {
        this.multitapeIndex = multitapeIndex;
    }

    /**
     * label_size: 2 int: Size of the tape label in bytes
     * @return
     */
    public short getLabelSize() {
        return labelSize;
    }

    /**
     * label_size: 2 int: Size of the tape label in bytes
     * @param labelSize
     */
    public void setLabelSize(short labelSize) {
        this.labelSize = labelSize;
    }

    /**
     * label: 0:32767 string: A user-defined, human readable, label for the tape (Or some other stuff, as long as it can be stringed. I'm a doc,not a cop)
     * @return
     */
    public String getLabel() {
        return label;
    }

    /**
     * label: 0:32767 string: A user-defined, human readable, label for the tape (Or some other stuff, as long as it can be stringed. I'm a doc,not a cop)
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * volume_id_size: 1 int: Size of the tape volume_id in bytes
     * @return
     */
    public byte getVolumeIdSize() {
        return volumeIdSize;
    }

    /**
     * volume_id_size: 1 int: Size of the tape volume_id in bytes
     * @param volumeIdSize
     */
    public void setVolumeIdSize(byte volumeIdSize) {
        this.volumeIdSize = volumeIdSize;
    }

    /**
     * volume_id: 0:126 string: ID of the volume, for use with multitape volumes
     * @return
     */
    public UUID getVolumeId() {
        return volumeId;
    }

    /**
     * volume_id: 0:126 string: ID of the volume, for use with multitape volumes
     * @param volumeId
     */
    public void setVolumeId(UUID volumeId) {
        this.volumeId = volumeId;
    }

    /**
     * header_metadata: 8 int: Additional metadata flags, giving information on how the tape was written (See metadata section for more info)
     * @return
     */
    public HeaderMetaDataField getHeaderMetadata() {
        return headerMetadata;
    }

    /**
     * header_metadata: 8 int: Additional metadata flags, giving information on how the tape was written (See metadata section for more info)
     * @param headerMetadata
     */
    public void setHeaderMetadata(HeaderMetaDataField headerMetadata) {
        this.headerMetadata = headerMetadata;
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        String volumeId = this.volumeId.toString();
        volumeIdSize = (byte) sizeof(volumeId);
        labelSize = (short) sizeof(label);

        callback.onStartWriteHeaderSection(this);
        writeAndCheck(toBytes(MAGIC), os);
        writeAndCheck(toBytes(version), os);
        writeAndCheck(toBytes(minVersion), os);
        writeAndCheck(toBytes(allowMultitape), os);
        writeAndCheck(toBytes(multitapeIndex), os);
        writeAndCheck(toBytes(headerMetadata.getHeaderMetadata()), os);
        writeAndCheck(toBytes(volumeIdSize), os);
        writeAndCheck(toBytes(volumeId), os);
        writeAndCheck(toBytes(labelSize), os);
        writeAndCheck(toBytes(label), os);
        callback.onEndWriteHeaderSection(this);

        return os;
    }

    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        callback.onStartReadHeaderSection();
        String magic = readNextString(is, MAGIC.getBytes(StandardCharsets.UTF_8).length);
        if (!MAGIC.equals(magic)) {
            // Not a ETFS tape! ABORT MISSION!
            IOException e = new IOException("Filesystem is not ETFS. Magic is \""+magic+"\", but should be \""+MAGIC+"\"");
            callback.onReadAbort(ReadAbortReason.WRONG_TAPE_FORMAT, e, MAGIC, magic);
            throw e;
        }

        this.version = readNextInt(is);
        minVersion = readNextInt(is);

        if (minVersion > EtfsCoreConfiguration.BINARY_VERSION) {
            IOException e = new IOException("The filesystem version is too new to be read by this ETFS client. Minimum required version is "+minVersion+", but this is version "+EtfsCoreConfiguration.BINARY_VERSION);
            callback.onReadAbort(ReadAbortReason.INCOMPATIBLE_VERSION, e, EtfsCoreConfiguration.BINARY_VERSION, minVersion);
            throw e;
        }

        allowMultitape = readNextByte(is) > 0;
        multitapeIndex = readNextShort(is);
        headerMetadata = new HeaderMetaDataField(readNextLong(is));
        volumeIdSize = readNextByte(is);
        volumeId = UUID.fromString(readNextString(is, volumeIdSize));
        labelSize = readNextShort(is);
        label = readNextString(is, labelSize);
        callback.onEndReadHeaderSection(this);

        return is;
    }
}
