package de.notepass.notaping.core.fs.etfs.data.runtime;

import de.notepass.notaping.core.fs.etfs.action.impl.AcceptAllFileSelector;
import de.notepass.notaping.core.fs.etfs.action.impl.NoopTapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.action.model.FileSelector;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.action.model.TapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.data.model.section.FileSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.PluginRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class EtfsReadDirectionBuilder {
    private List<EtfsPlugin> plugins = new LinkedList<>();
    private PathRemapper mapper;
    private TapeSpecificOperationDispatcher opDispatch;
    private static final Logger LOGGER = LoggerFactory.getLogger(EtfsReadDirectionBuilder.class);
    private FileSelector fileSelector;

    /**
     * Creates a new instance of the factory and passes the required data
     * @return
     */
    public static EtfsReadDirectionBuilder newInstance(PathRemapper mapper) {
        EtfsReadDirectionBuilder b = new EtfsReadDirectionBuilder();
        b.mapper = mapper;
        return b;
    }

    public EtfsReadDirectionBuilder addPlugin(EtfsPlugin plugin) {
        plugins.add(plugin);
        return this;
    }

    public EtfsReadDirectionBuilder addPlugins(EtfsPlugin ... plugin) {
        Collections.addAll(plugins, plugin);
        return this;
    }

    public EtfsReadDirectionBuilder withPathRemapper(PathRemapper mapper) {
        this.mapper = mapper;
        return this;
    }

    public EtfsReadDirectionBuilder withTapeSpecificOperationDispatcher(TapeSpecificOperationDispatcher opd) {
        opDispatch = opd;
        return this;
    }

    public EtfsReadDirectionBuilder withFileSelector(FileSelector fs) {
        fileSelector = fs;
        return this;
    }

    public EtfsInstance build() {
        EtfsInstance instance = new EtfsInstance();
        instance.setPluginRegistry(new PluginRegistry());
        instance.getLogicalTapeInfo().setHeaderSection(new HeaderSection());
        instance.getLogicalTapeInfo().setPluginConfigSection(new PluginConfigSection());
        instance.getLogicalTapeInfo().setTableOfContentSection(new TableOfContentSection());
        instance.getLogicalTapeInfo().setFileSection(new FileSection());
        instance.setPathRemapper(mapper);
        instance.setTapeIndex((short) 0);

        for (EtfsPlugin plugin : plugins) {
            instance.getPluginRegistry().register(plugin);
        }

        if (opDispatch == null) {
            // If not OPD is given, fall back to noop (Removes EOF functionality)
            LOGGER.warn("TapeSpecificOperationDispatcher is null. Falling back to NOOP implementation.");
            opDispatch = new NoopTapeSpecificOperationDispatcher();
        }

        if (fileSelector == null) {
            LOGGER.warn("No FileSelector given. Using default AcceptAllFileSelector. All files will be restored.");
            fileSelector = new AcceptAllFileSelector();
        }

        instance.setOpDispatcher(opDispatch);
        instance.setFileSelector(fileSelector);

        // Return result
        return instance;
    }
}
