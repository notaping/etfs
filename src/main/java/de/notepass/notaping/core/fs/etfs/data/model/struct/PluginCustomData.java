package de.notepass.notaping.core.fs.etfs.data.model.struct;

import de.notepass.notaping.core.fs.etfs.EtfsException;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.metadata.PluginMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.*;

import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.toBytes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Java class representation of the "PluginCustomData" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class PluginCustomData implements EtfsSerializableElement {
    // internal_id: 2 int: Reference to the internal ID from the header section
    private short internalId;

    // content_size: 4 int: Size of the content field in bytes
    private int contentSize;

    // The data itself is not stored here, as it may be multiple GB per plugin. The plugin will later
    // get a reference to the tape stream from which it can read data and work with it in an optimised
    // way
    // content: 2147483000 byte: Custom data content. Advised to be string, but can be any data format

    // plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
    private PluginMetaDataField pluginDataMetadata = new PluginMetaDataField();

    // ========================== THE FOLLOWING DATA IS NOT SERIALIZED TO THE TAPE AND ONLY NEEDED FOR PROCESSING ==========================

    // Position of the metadata entry on the tape
    private MetadataPosition position;

    // Context element passed to the plugin when writing
    private Object[] context = new Object[0];

    // Cached temp files for write direction: AFTER_HEADER and AFTER_TOC_ENTRY
    private Map<MetadataPosition, Path> cacheableMetadataTempFiles = new HashMap<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(PluginCustomData.class);

    /**
     * Constructor with base information on the PCD position/context
     * @param position Position on the tape
     * @param context Element which is passed to the plugin as "context" element
     */
    public PluginCustomData(MetadataPosition position, Object ... context) {
        this.position = position;
        this.context = context;
    }

    /**
     * Returns the "size" field, which contains the number of bytes in the content field
     * @return
     */
    public int getContentSize() {
        return contentSize;
    }

    /**
     * Sets the "size" field, which contains the number of bytes in the content field
     * @param contentSize
     */
    public void setContentSize(int contentSize) {
        this.contentSize = contentSize;
    }

    /**
     * Returns the remapped ID for the plugin which the data belongs to. The real IDs will be
     * remapped to short-values to save space. The real plugin-ID can be found in the Header part<br/>
     * You can access all plugin information by calling get() on {@link EtfsInstance#getPluginRegistry()}. You cannot
     * access this information if you do not have the ETFS instance this is associated with, as the short ID is specific
     * to a given ETFS instance
     * @return
     */
    public short getInternalId() {
        return internalId;
    }

    /**
     * Sets the remapped ID for the plugin which the data belongs to. The real IDs will be
     * remapped to short-values to save space. The real plugin-ID can be found in the Header part<br/>
     * ou can access all plugin information by calling get() on {@link EtfsInstance#getPluginRegistry()}. You cannot
     * ccess this information if you do not have the ETFS instance this is associated with, as the short ID is specific
     * o a given ETFS instance
     * @return
     */
    public void setInternalId(short internalId) {
        this.internalId = internalId;
    }

    /**
     * Returns the position of the PCD element on the tape
     * @return
     */
    public MetadataPosition getPosition() {
        return position;
    }

    /**
     * Returns the position of the PCD element on the tape
     * @return
     */
    public void setPosition(MetadataPosition position) {
        this.position = position;
    }

    /**
     * Returns the element which is passed to the plugin as "context" element
     * @return
     */
    public Object[] getContext() {
        return context;
    }

    /**
     * Sets the element which is passed to the plugin as "context" element
     * @param context
     */
    public void setContext(Object ... context) {
        this.context = context;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @return
     */
    public PluginMetaDataField getPluginDataMetadata() {
        return pluginDataMetadata;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @param pluginDataMetadata
     */
    public void setPluginDataMetadata(PluginMetaDataField pluginDataMetadata) {
        this.pluginDataMetadata = pluginDataMetadata;
    }

    /**
     * Casts the classes for using the correct "wantsToWritePluginData" method and extracts the metadata
     * @return
     */
    private boolean dispatchWriteCheck(EtfsInstance etfs) {
        switch (position) {
            case AFTER_FILE_CONTENT:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 2 of 2 context elements to pass to plugin (FileContent and File). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, null, null);
                } else if (context.length == 1) {
                    LOGGER.warn("Missing 1 of 2 context elements to pass to plugin (File). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileContent) context[0], null);
                } else {
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileContent) context[0], (Path) context[1]);
                }
            case AFTER_HEADER:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (HeaderSection). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (HeaderSection) null);
                } else if (context.length == 1) {
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (HeaderSection) context[0]);
                }
            case AFTER_TOC_ENTRY:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (FileEntry). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileEntry) null);
                } else if (context.length == 1) {
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileEntry) context[0]);
                }
            default:
                LOGGER.warn("Unknown PluginCustomData-Position for mapping dispatchWriteCheck: " + position);
                return false;
        }
    }

    /**
     * Casts the classes for using the correct "writePluginData" method and extracts the metadata
     * @return
     */
    private void dispatchWriteCall(EtfsInstance etfs, OutputStream os) throws IOException {
        switch (position) {
            case AFTER_FILE_CONTENT:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 2 of 2 context elements to pass to plugin (FileContent and File). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, null, null, os);
                } else if (context.length == 1) {
                    LOGGER.warn("Missing 1 of 2 context elements to pass to plugin (File). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileContent) context[0], null, os);
                } else {
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileContent) context[0], (Path) context[1], os);
                }
                break;
            case AFTER_HEADER:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (HeaderSection). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (HeaderSection) null, os);
                } else {
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (HeaderSection) context[0], os);
                }
                break;
            case AFTER_TOC_ENTRY:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (FileEntry). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileEntry) null, os);
                } else {
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileEntry) context[0], os);
                }
                break;
            default:
                LOGGER.warn("Unknown PluginCustomData-Position for mapping dispatchWriteCall: " + position);
        }
    }

    /**
     * Casts the classes for using the correct "readPluginData" method and extracts the metadata
     * @return
     */
    private void dispatchReadCall(EtfsInstance etfs, InputStream is, int version) throws IOException {
        switch (position) {
            case AFTER_FILE_CONTENT:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 2 of 2 context elements to pass to plugin (FileContent and File). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).readPluginData(position, null, null, is, version, contentSize);
                } else if (context.length == 1) {
                    LOGGER.warn("Missing 1 of 2 context elements to pass to plugin (File). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).readPluginData(position, (FileContent) context[0], null, is, version, contentSize);
                } else {
                    etfs.getPluginRegistry().get(internalId).readPluginData(position, (FileContent) context[0], (Path) context[1], is, version, contentSize);
                }
                break;
            case AFTER_HEADER:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (HeaderSection). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).readPluginData(position, (HeaderSection) null, is, version, contentSize);
                } else {
                    etfs.getPluginRegistry().get(internalId).readPluginData(position, (HeaderSection) context[0], is, version, contentSize);
                }
                break;
            case AFTER_TOC_ENTRY:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (FileEntry). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).readPluginData(position, (FileEntry) null, is, version, contentSize);
                } else {
                    etfs.getPluginRegistry().get(internalId).readPluginData(position, (FileEntry) context[0], is, version, contentSize);
                }
                break;
            default:
                LOGGER.warn("Unknown PluginCustomData-Position for mapping dispatchWriteCall: " + position);
        }
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        EtfsPlugin plugin = context.getPluginRegistry().get(internalId);

        callback.onStartWritePluginCustomData(position, plugin);
        Path pcdTempFile;
        boolean pluginError = false;
        if (!isMultitapePart || position == MetadataPosition.AFTER_FILE_CONTENT) {
            // Step 1: Write plugin data to temporary file, as the size needs to be known before writing to tape
            pcdTempFile = getTempFile();
            cacheableMetadataTempFiles.put(position, pcdTempFile);
            try (OutputStream fos = Files.newOutputStream(pcdTempFile)) {
                dispatchWriteCall(context, fos);
            } catch (Exception e) {
                Files.delete(pcdTempFile);

                if (e instanceof IOException) {
                    throw e;
                } else {
                    // TODO: [Blocker] Error flag
                    LOGGER.error("Unhandled exception in plugin \"" + plugin.getID() + "\", no custom data will be written", e);
                    callback.onUnhandledPluginException(plugin, e);
                    pluginError = true;
                    pluginDataMetadata.setFlag(PluginMetaDataField.PLUGIN_METADATA_FLAG.PLUGIN_ERROR);
                }
            }
        } else {
            // Use cached copy if not on first tape in multitape volume or a position where no caching is possible (outside header section)
            pcdTempFile = cacheableMetadataTempFiles.get(position);

            // If the file reference is null or the file doesn't exist on disk, there probably was an error when creating it the firs time
            pluginError = pcdTempFile == null || !Files.exists(pcdTempFile);

            if (pluginError) {
                LOGGER.info("Temporary plugin custom data entry file "+pcdTempFile+" does not exist for reading it back in multitape volume. Probably deleted due to previous errors");
            }
        }

        // Step 2: Prepare values
        if (!pluginError) {
            contentSize = (int) Files.size(pcdTempFile);
        } else {
            contentSize = 0;
        }

        // Step 3: Write values
        writeAndCheck(toBytes(contentSize), os);
        writeAndCheck(toBytes(internalId), os);
        if (!pluginError) {
            try (InputStream is = Files.newInputStream(pcdTempFile)) {
                is.transferTo(os);
            } finally {
                if (position == MetadataPosition.AFTER_FILE_CONTENT) {
                    // Only delete non-cacheable plugin data at runtime
                    Files.delete(pcdTempFile);
                }
            }
        }

        writeAndCheck(toBytes(pluginDataMetadata.getPluginDataMetadata()), os);

        callback.onEndWritePluginCustomData(position, plugin);

        return os;
    }

    /**
     * handles tape switching logic
     * @return true if the user inserted the correct tape and processing can just continue, false if
     */
    private boolean handleTapeSwitch(EtfsInstance context, EtfsReadProgressCallback callback, EtfsPlugin plugin, InputStream is, MetadataPosition position) throws IOException {
        if (position != MetadataPosition.AFTER_FILE_CONTENT) {
            // Still in header, tape cannot be split here
            throw new EtfsException("Tape ran out of space before files section. The headers cannot be split over multiple tapes, so no multitape is offered", EtfsException.ErrorCode.MISC_ERROR);
        }

        try {
            context.switchMutitapeTape(is, callback);
            return true;
        } catch (EtfsException e) {
            if (e.getErrorCode() == EtfsException.ErrorCode.MULTITAPE_NOT_NEXT_TAPE) {
                // User forces us to use a different tape. Abort data processing
                callback.onEndReadPluginCustomData(position, plugin);
                return false;
            } else {
                // Other error, not handable here
                throw e;
            }
        }
    }

    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        contentSize = readNextInt(is);
        internalId = readNextShort(is);
        pluginDataMetadata = new PluginMetaDataField(readNextInt(is));

        EtfsPlugin plugin = context.getPluginRegistry().get(getInternalId());
        long skipSize = contentSize;

        if (ignorePluginSections) {
            if (is.skip(skipSize) != skipSize) {
                // End of tape, tape switching needed
                boolean correctTapeInserted = handleTapeSwitch(context, callback, plugin, is, position);
                // Get new InputStream
                is = context.getOpDispatcher().getCurrentInputStream();
                if (!correctTapeInserted) {
                    // Tape with wrong index inserted, abort read
                    return is;
                }
            }
            return is;
        }

        Optional<PluginInfo> infoEntry = context
                .getLogicalTapeInfo()
                .getPluginConfigSection()
                .getPluginInfo()
                .stream()
                .filter(pluginInfo -> pluginInfo.getInternalId() == internalId)
                .findFirst();

        // If the plugin is not available, skip reading data and terminate
        if (plugin == null) {
            if (infoEntry.isPresent()) {
                callback.onMissingPluginDiscovered(String.valueOf(infoEntry.get().getId()));
                LOGGER.warn("No local plugin available for ID " +infoEntry.get().getId());
            } else {
                callback.onMissingPluginDiscovered(String.valueOf(internalId));
                LOGGER.warn("No local plugin available for internal ID " +internalId);
            }

            if (is.skip(skipSize) != skipSize) {
                // End of tape, tape switching needed
                boolean correctTapeInserted = handleTapeSwitch(context, callback, plugin, is, position);
                // Get new InputStream
                is = context.getOpDispatcher().getCurrentInputStream();
                if (!correctTapeInserted) {
                    // Tape with wrong index inserted, abort read
                    return is;
                }
            }

            return is;
        }

        callback.onStartReadPluginCustomData(position, plugin);
        // TODO: [Low] Increase read performance by giving a threshold until which data is read to memory via byte array streams
        if (!pluginDataMetadata.getFlag(PluginMetaDataField.PLUGIN_METADATA_FLAG.PLUGIN_ERROR)) {
            // Copy data over to file. This avoids problem with the plugin reading too much/too little data
            Path tempFile = getTempFile();
            int remainingBytes = contentSize;
            try (OutputStream tempFileStream = Files.newOutputStream(tempFile)) {
                byte[] buffer = new byte[getPreferredReadBufferSize()];
                int numBytesRead;
                while (remainingBytes > 0) {
                    if (remainingBytes >= buffer.length) {
                        numBytesRead = is.read(buffer);
                    } else {
                        buffer = new byte[remainingBytes];
                        numBytesRead = is.read(buffer);
                    }

                    tempFileStream.write(buffer);
                    remainingBytes -= numBytesRead;

                    if (numBytesRead != buffer.length) {
                        // End of tape, tape switching needed
                        boolean correctTapeInserted = handleTapeSwitch(context, callback, plugin, is, position);
                        // Get new InputStream
                        is = context.getOpDispatcher().getCurrentInputStream();
                        if (!correctTapeInserted) {
                            // Tape with wrong index inserted, abort read
                            return is;
                        }
                    }
                }
            } catch (IOException e) {
                LOGGER.error("Could not transfer custom data for plugin "+plugin.getID()+" to temporary file "+tempFile+". Plugin will be skipped.");
                is.skip(remainingBytes);
                return is;
            }

            try (InputStream fis = Files.newInputStream(tempFile)) {
                // Optional should always exist, as the data for initializing plugins was read from the PluginInfo section
                dispatchReadCall(context, fis, infoEntry.get().getVersion());
            } finally {
                // Make sure that the temp file is deleted
                Files.delete(tempFile);
                callback.onEndReadPluginCustomData(position, plugin);
            }
        } else {
            LOGGER.warn("Custom data for plugin \""+context.getPluginRegistry().get(getInternalId()).getID()+"\" not readable: Plugin threw error when writing");
        }

        return is;
    }
}
