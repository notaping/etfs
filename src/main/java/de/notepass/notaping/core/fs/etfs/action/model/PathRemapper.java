package de.notepass.notaping.core.fs.etfs.action.model;

import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;

import java.nio.file.Path;
import java.util.List;

public interface PathRemapper {
    public void prepareFileMapping(List<FileEntry> allFiles) ;
    public Path map(String normalizedPath);
}
