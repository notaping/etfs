package de.notepass.notaping.core.fs.etfs.data.model.section;

import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileContent;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FileSection implements EtfsSerializableElement {
    // header_size_bytes: 8 int: Number of bytes written until here
    private long headerSizeBytes;

    // files: 0:9223372036854775807 List<FileContent>: List of files
    private List<FileContent> files = new ArrayList<>();

    /**
     * files: 0:9223372036854775807 List<FileContent>: List of files
     * @return
     */
    public List<FileContent> getFiles() {
        return files;
    }

    /**
     * files: 0:9223372036854775807 List<FileContent>: List of files
     * @param files
     */
    public void setFiles(List<FileContent> files) {
        this.files = files;
    }

    /**
     * header_size_bytes: 8 int: Number of bytes written until here
     * @return
     */
    public long getHeaderSizeBytes() {
        return headerSizeBytes;
    }

    /**
     * header_size_bytes: 8 int: Number of bytes written until here
     * @param headerSizeBytes
     */
    public void setHeaderSizeBytes(long headerSizeBytes) {
        this.headerSizeBytes = headerSizeBytes;
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        callback.onStartWriteFileSection(files.size());
        for (FileContent fc:files) {
            os = fc.serialize(os, isMultitapePart, context, callback);
        }
        callback.onEndWriteFileSection();

        return os;
    }

    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        callback.onStartReadFileSection(context.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().size());
        long bytesToSkip = 0;
        for (FileEntry tocEntry : context.getLogicalTapeInfo().getTableOfContentSection().getFileEntries()) {
            if (tocEntry.isRestore()) {
                if (bytesToSkip > 0) {
                    // Previous file content is to skip. Skip now
                    // This is done here, so that skips can be accumulated to save time when changing positions
                    long skipped = is.skip(bytesToSkip);

                    while (skipped < bytesToSkip) {
                        // Request tape change, as not enough data was skipped, indicating end of tape
                        is = context.switchMutitapeTape(is, callback);
                        skipped += is.skip(bytesToSkip);
                    }

                    bytesToSkip = 0;
                }

                FileContent fc = new FileContent();
                fc.setFileEntryData(tocEntry);
                files.add(fc);
                is = fc.deserialize(is, version, ignorePluginSections, context, callback);
            } else {
                // Skip file
                bytesToSkip += tocEntry.getOverallFileContentEntrySize();
            }

        }
        callback.onEndReadFileSection();

        return is;
    }
}
