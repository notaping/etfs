package de.notepass.notaping.core.fs.etfs.data.model.section;

import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.model.struct.PluginConfigEntry;
import de.notepass.notaping.core.fs.etfs.data.model.struct.PluginCustomData;
import de.notepass.notaping.core.fs.etfs.data.model.struct.PluginInfo;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Java class representation of the "PluginConfig" section, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class PluginConfigSection implements EtfsSerializableElement {
    // plugin_info_length: 2 int: Number of entries in the plugin_info list
    private short pluginInfoLength;

    // plugin_info: 0:32767 list<PluginInfo>: The list of plugins used when creating this tape and configuration metadata for them
    private List<PluginInfo> pluginInfo = new ArrayList<>();

    // entries_length: 2 int: Number of entries in the entries list
    private short entriesLength;

    // entries: 0:32767 list<PluginConfigEntry>: List of configuration entries
    private List<PluginConfigEntry> entries = new ArrayList<>();

    // plugin_data_length: 2 int: Number of entries in the entries list
    private short pluginDataLength;

    // plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_HEADER)
    private List<PluginCustomData> pluginData = new ArrayList<>();

    /**
     * plugin_info_length: 2 int: Number of entries in the plugin_info list
     * @return
     */
    public short getPluginInfoLength() {
        return pluginInfoLength;
    }

    /**
     * plugin_info_length: 2 int: Number of entries in the plugin_info list
     * @param pluginInfoLength
     */
    public void setPluginInfoLength(short pluginInfoLength) {
        this.pluginInfoLength = pluginInfoLength;
    }

    /**
     * plugin_info: 0:32767 list<PluginInfo>: The list of plugins used when creating this tape and configuration metadata for them
     * @return
     */
    public List<PluginInfo> getPluginInfo() {
        return pluginInfo;
    }

    /**
     * plugin_info: 0:32767 list<PluginInfo>: The list of plugins used when creating this tape and configuration metadata for them
     * @param pluginInfo
     */
    public void setPluginInfo(List<PluginInfo> pluginInfo) {
        this.pluginInfo = pluginInfo;
    }

    /**
     * entries_length: 2 int: Number of entries in the entries list
     * @return
     */
    public short getEntriesLength() {
        return entriesLength;
    }

    /**
     * entries_length: 2 int: Number of entries in the entries list
     * @param entriesLength
     */
    public void setEntriesLength(short entriesLength) {
        this.entriesLength = entriesLength;
    }

    /**
     * entries: 0:32767 list<PluginConfigEntry>: List of configuration entries
     * @return
     */
    public List<PluginConfigEntry> getEntries() {
        return entries;
    }

    /**
     * entries: 0:32767 list<PluginConfigEntry>: List of configuration entries
     * @param entries
     */
    public void setEntries(List<PluginConfigEntry> entries) {
        this.entries = entries;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the entries list
     * @return
     */
    public short getPluginDataLength() {
        return pluginDataLength;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the entries list
     * @param pluginDataLength
     */
    public void setPluginDataLength(short pluginDataLength) {
        this.pluginDataLength = pluginDataLength;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_HEADER)
     * @return
     */
    public List<PluginCustomData> getPluginData() {
        return pluginData;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_HEADER)
     * @param pluginData
     */
    public void setPluginData(List<PluginCustomData> pluginData) {
        this.pluginData = pluginData;
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        pluginInfoLength = (short) getPluginInfo().size();

        callback.onStartWritePluginConfigSection(this);
        writeAndCheck(toBytes(pluginInfoLength), os);
        for (PluginInfo pi : getPluginInfo()) {
            os = pi.serialize(os, isMultitapePart, context, callback);
        }

        if (!isMultitapePart) {
            // Only create local config entry references on the first tape call
            for (EtfsPlugin plugin : context.getPluginRegistry().getActivePlugins()) {
                if (plugin.wantsToWriteRequiredConfigForReadOperation()) {
                    PluginConfigEntry pce = new PluginConfigEntry();
                    pce.setInternalId(context.getPluginRegistry().getInternalId(plugin));
                    entries.add(pce);
                }
            }
        }
        entriesLength = (short) getEntries().size();
        writeAndCheck(toBytes(entriesLength), os);
        for (PluginConfigEntry pce : getEntries()) {
            os = pce.serialize(os, isMultitapePart, context, callback);
        }

        callback.onStartWritePluginCustomDataSection(MetadataPosition.AFTER_HEADER, pluginDataLength);
        if (!isMultitapePart) {
            // Only create local custom data entry references on the first tape call
            for (EtfsPlugin plugin : context.getPluginRegistry().getActivePlugins()) {
                if (plugin.wantsToWritePluginData(MetadataPosition.AFTER_HEADER, context.getLogicalTapeInfo().getHeaderSection())) {
                    // Only write data the plugin wants to write and if this is the first header
                    PluginCustomData pcd = new PluginCustomData(MetadataPosition.AFTER_HEADER, context.getLogicalTapeInfo().getHeaderSection());
                    pluginData.add(pcd);
                }
            }
        }

        pluginDataLength = (short) getPluginData().size();
        writeAndCheck(toBytes(pluginDataLength), os);
        for (PluginCustomData pcd : getPluginData()) {
            os = pcd.serialize(os, isMultitapePart, context, callback);
        }
        callback.onEndWritePluginCustomDataSection(MetadataPosition.AFTER_HEADER);

        callback.onEndWritePluginConfigSection(this);

        return os;
    }

    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        pluginInfoLength = readNextShort(is);
        for (short i = 0; i < pluginInfoLength; i++) {
            PluginInfo pi = new PluginInfo();
            pluginInfo.add(pi);
            is = pi.deserialize(is, version, ignorePluginSections, context, callback);
        }

        if (!ignorePluginSections) {
            context.getPluginRegistry().buildShortIdCache(pluginInfo);
        }

        entriesLength = readNextShort(is);
        callback.onStartReadPluginConfigSection(entriesLength);
        for (short i = 0; i < entriesLength; i++) {
            PluginConfigEntry pce = new PluginConfigEntry();
            entries.add(pce);
            is = pce.deserialize(is, version, ignorePluginSections, context, callback);
        }

        pluginDataLength = readNextShort(is);
        callback.onStartReadPluginCustomDataSection(MetadataPosition.AFTER_HEADER, pluginDataLength);
        for (short i = 0; i < pluginDataLength; i++) {
            PluginCustomData pcd = new PluginCustomData(MetadataPosition.AFTER_HEADER, context.getLogicalTapeInfo().getHeaderSection());
            pluginData.add(pcd);
            is = pcd.deserialize(is, version, ignorePluginSections, context, callback);
        }
        callback.onEndReadPluginCustomDataSection(MetadataPosition.AFTER_HEADER);

        callback.onEndReadPluginConfigSection(this);

        return is;
    }
}
