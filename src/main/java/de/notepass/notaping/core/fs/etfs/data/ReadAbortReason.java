package de.notepass.notaping.core.fs.etfs.data;

/**
 * Enum to track why reading an ETFS instance was aborted
 */
public enum ReadAbortReason {
    /**
     * Indicates that the tape is not a ETFS tape
     */
    WRONG_TAPE_FORMAT,

    /**
     * The min_version on the tape is bigger than the current ETFS core version
     */
    INCOMPATIBLE_VERSION,

    /**
     * Indicates an unhandled internal error
     */
    INTERNAL_ERROR,

    /**
     * Indicates that the user aborted the process
     */
    USER_REQUEST;
}
