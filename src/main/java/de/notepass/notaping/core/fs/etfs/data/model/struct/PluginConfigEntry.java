package de.notepass.notaping.core.fs.etfs.data.model.struct;

import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.metadata.PluginMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.toBytes;

/**
 * Java class representation of the "PluginConfigEntry" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class PluginConfigEntry implements EtfsSerializableElement {
    // internal_id: 2 int: Reference to the internal ID from the header section
    private short internalId;

    // content_size: 4 int: Size of the content field in bytes
    private int contentSize;

    // The data itself is not stored here, as it may be multiple GB per plugin. The plugin will later
    // get a reference to the tape stream from which it can read data and work with it in an optimised
    // way
    // content: 2147483000 byte: Configuration file content. Advised to be string, but can be any data format

    // plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
    private PluginMetaDataField pluginDataMetadata = new PluginMetaDataField();

    private Path pceTempFile;

    private static final Logger LOGGER = LoggerFactory.getLogger(PluginConfigEntry.class);


    /**
     * content_size: 4 int: Size of the content field in bytes
     * @return
     */
    public int getContentSize() {
        return contentSize;
    }

    /**
     * content_size: 4 int: Size of the content field in bytes
     * @param contentSize
     */
    public void setContentSize(int contentSize) {
        this.contentSize = contentSize;
    }

    /**
     * internal_id: 2 int: Reference to the internal ID from the header section
     * @return
     */
    public short getInternalId() {
        return internalId;
    }

    /**
     * internal_id: 2 int: Reference to the internal ID from the header section
     * @param internalId
     */
    public void setInternalId(short internalId) {
        this.internalId = internalId;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @return
     */
    public PluginMetaDataField getPluginDataMetadata() {
        return pluginDataMetadata;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @param pluginDataMetadata
     */
    public void setPluginDataMetadata(PluginMetaDataField pluginDataMetadata) {
        this.pluginDataMetadata = pluginDataMetadata;
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        EtfsPlugin plugin = context.getPluginRegistry().get(internalId);

        callback.onStartWritePluginConfiguration(plugin);

        boolean pluginError = false;

        if (!isMultitapePart) {
            // Step 1: Write plugin data to temporary file, as the size needs to be known before writing to tape
            pceTempFile = getTempFile();
            try (OutputStream fos = Files.newOutputStream(pceTempFile)) {
                plugin.writeRequiredConfigForReadOperation(fos);
            } catch (Exception e) {
                Files.delete(pceTempFile);

                if (e instanceof IOException) {
                    throw e;
                } else {
                    LOGGER.error("Unhandled exception in plugin \"" + plugin.getID() + "\", no config data will be written", e);
                    pluginError = true;
                    callback.onUnhandledPluginException(plugin, e);
                    pluginDataMetadata.setFlag(PluginMetaDataField.PLUGIN_METADATA_FLAG.PLUGIN_ERROR);
                }
            }
        } else {
            // Use cached plugin data when writing multitape volume

            if (!Files.exists(pceTempFile)) {
                // Temporary file doesn't exists, meaning that there was an unhandeled exception previously
                LOGGER.info("Temporary plugin config entry file "+pceTempFile+" does not exist for reading it back in multitape volume. Probably deleted due to previous errors");
                pluginError = true;
            }
        }

        // Step 2: Prepare values
        if (!pluginError) {
            contentSize = (int) Files.size(pceTempFile);
        } else {
            contentSize = 0;
        }

        // Step 3: Write values
        writeAndCheck(toBytes(contentSize), os);
        writeAndCheck(toBytes(internalId), os);

        if (!pluginError) {
            try (InputStream is = Files.newInputStream(pceTempFile)) {
                is.transferTo(os);
            }
        }

        writeAndCheck(toBytes(pluginDataMetadata.getPluginDataMetadata()), os);

        callback.onEndWritePluginConfiguration(plugin);

        return os;
    }

    //TODO: [Low] This is the same as PCD data reading (Except for the plugin dispatch call). Refactor and put into single method
    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        EtfsPlugin plugin = context.getPluginRegistry().get(getInternalId());

        contentSize = readNextInt(is);
        internalId = readNextShort(is);
        pluginDataMetadata = new PluginMetaDataField(readNextInt(is));
        long skipSize = contentSize;

        if (ignorePluginSections) {
            is.skip(skipSize);
            return is;
        }

        Optional<PluginInfo> infoEntry = context
                .getLogicalTapeInfo()
                .getPluginConfigSection()
                .getPluginInfo()
                .stream()
                .filter(pluginInfo -> pluginInfo.getInternalId() == internalId)
                .findFirst();

        // If the plugin is not available, skip reading data and terminate
        if (plugin == null) {
            if (infoEntry.isPresent()) {
                callback.onMissingPluginDiscovered(infoEntry.get().getId());
                LOGGER.warn("No local plugin available for ID " +infoEntry.get().getId());
            } else {
                callback.onMissingPluginDiscovered(String.valueOf(internalId));
                LOGGER.warn("No local plugin available for internal ID " +internalId);
            }

            is.skip(skipSize);
            return is;
        }

        callback.onStartReadPluginConfiguration(plugin);
        // TODO: [Low] Increase read performance by giving a threshold until which data is read to memory via byte array streams
        if (!pluginDataMetadata.getFlag(PluginMetaDataField.PLUGIN_METADATA_FLAG.PLUGIN_ERROR)) {
            // Copy data over to file. This avoids problem with the plugin reading too much/too little data
            Path tempFile = getTempFile();
            int remainingBytes = contentSize;
            try (OutputStream tempFileStream = Files.newOutputStream(tempFile)) {
                byte[] buffer = new byte[getPreferredReadBufferSize()];
                while (remainingBytes > 0) {
                    if (remainingBytes >= buffer.length) {
                        is.read(buffer);
                    } else {
                        buffer = new byte[remainingBytes];
                        is.read(buffer);
                    }

                    tempFileStream.write(buffer);
                    remainingBytes -= buffer.length;
                }
            } catch (IOException e) {
                LOGGER.error("Could not transfer custom data for plugin "+plugin.getID()+" to temporary file "+tempFile+". Plugin will be skipped.");
                is.skip(remainingBytes);
                return is;
            }

            try (InputStream fis = Files.newInputStream(tempFile)) {
                // Optional should always exist, as the data for initializing plugins was read from the PluginInfo section
                plugin.readRequiredConfigForReadOperation(fis, infoEntry.get().getVersion(), contentSize);
            } finally {
                callback.onEndReadPluginConfiguration(plugin);
            }
        } else {
            LOGGER.warn("Custom data for plugin \""+context.getPluginRegistry().get(getInternalId()).getID()+"\" not readable: Plugin threw error when writing");
        }

        return is;
    }
}
