package de.notepass.notaping.core.fs.etfs.data.runtime;

import de.notepass.notaping.core.fs.etfs.action.model.FileSelector;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.action.model.TapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.data.model.section.FileSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;

public class LogicalTapeInfo {
    // Main FS sections
    private HeaderSection headerSection;
    private PluginConfigSection pluginConfigSection;
    private TableOfContentSection tableOfContentSection;
    private FileSection fileSection;


    /**
     * Returns the main header section of the filesystem (if present)
     * @return
     */
    public HeaderSection getHeaderSection() {
        return headerSection;
    }

    /**
     * Sets the main header section of the filesystem
     */
    public void setHeaderSection(HeaderSection headerSection) {
        this.headerSection = headerSection;
    }

    /**
     * Returns the main plugin config section of the filesystem (if present)
     * @return
     */
    public PluginConfigSection getPluginConfigSection() {
        return pluginConfigSection;
    }

    /**
     * Sets the main plugin config section of the filesystem
     */
    public void setPluginConfigSection(PluginConfigSection pluginConfigSection) {
        this.pluginConfigSection = pluginConfigSection;
    }

    /**
     * Returns the main TOC section of the filesystem (if present)
     * @return
     */
    public TableOfContentSection getTableOfContentSection() {
        return tableOfContentSection;
    }

    /**
     * Sets the main TOC section of the filesystem
     */
    public void setTableOfContentSection(TableOfContentSection tableOfContentSection) {
        this.tableOfContentSection = tableOfContentSection;
    }

    /**
     * Returns the main files section of the filesystem (if present)
     * @return
     */
    public FileSection getFileSection() {
        return fileSection;
    }

    /**
     * Sets the main files section of the filesystem
     */
    public void setFileSection(FileSection fileSection) {
        this.fileSection = fileSection;
    }
}
