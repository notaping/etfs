package de.notepass.notaping.core.fs.etfs.data.model.struct;

import de.notepass.notaping.core.fs.etfs.EtfsException;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.metadata.FileRuntimeMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.toBytes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Java class representation of the "FileContent" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class FileContent implements EtfsSerializableElement {
    // file_content: 0:9223372036854775807 byte: Content of the file. Size is dictated by the file_size filed in the TOC section
    // This will not be read to memory

    //runtime_meta: 4 int: Additional metadata resulting of possible runtime problems when reading/writing the file (See Metadata section for more info)
    private FileRuntimeMetaDataField runtimeMeta = new FileRuntimeMetaDataField();

    // plugin_data_length: 2 int: Number of entries in the plugin_data list
    private short pluginDataLength;

    // plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_CONTENT)
    private List<PluginCustomData> pluginData = new ArrayList<>();

    // Reference to the FileEntry struct this file belongs to
    private FileEntry fileEntryData;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileContent.class);

    /**
     * Size in bytes of all static fields
     */
    public static final long BASE_SIZE = 2 + 4;

    /**
     * runtime_meta: 4 int: Additional metadata resulting of possible runtime problems when reading/writing the file (See Metadata section for more info)
     * @return
     */
    public FileRuntimeMetaDataField getRuntimeMeta() {
        return runtimeMeta;
    }

    /**
     * runtime_meta: 4 int: Additional metadata resulting of possible runtime problems when reading/writing the file (See Metadata section for more info)
     * @param runtimeMeta
     */
    public void setRuntimeMeta(FileRuntimeMetaDataField runtimeMeta) {
        this.runtimeMeta = runtimeMeta;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     * @return
     */
    public short getPluginDataLength() {
        return pluginDataLength;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     * @param pluginDataLength
     */
    public void setPluginDataLength(short pluginDataLength) {
        this.pluginDataLength = pluginDataLength;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_CONTENT)
     * @return
     */
    public List<PluginCustomData> getPluginData() {
        return pluginData;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_CONTENT)
     * @param pluginData
     */
    public void setPluginData(List<PluginCustomData> pluginData) {
        this.pluginData = pluginData;
    }

    /**
     * Reference to the FileEntry struct this file belongs to
     * @return
     */
    public FileEntry getFileEntryData() {
        return fileEntryData;
    }

    /**
     * Reference to the FileEntry struct this file belongs to
     * @param fileEntryData
     */
    public void setFileEntryData(FileEntry fileEntryData) {
        this.fileEntryData = fileEntryData;
    }

    List<PluginCustomData> generateWritingPluginCustomDataList(EtfsInstance context) {
        List<PluginCustomData> data = new ArrayList<>();
        for (EtfsPlugin plugin : context.getPluginRegistry().getActivePlugins()) {
            if (plugin.wantsToWritePluginData(MetadataPosition.AFTER_FILE_CONTENT, this, getFileEntryData().getTargetFile())) {
                data.add(new PluginCustomData(MetadataPosition.AFTER_FILE_CONTENT, this, getFileEntryData().getTargetFile()));
            }
        }

        return data;
    }

    void serializePluginCustomData(WrittenBytesAwareOutputStream os, List<PluginCustomData> customData, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        // WARNING: This method is also called by de.notepass.notaping.core.fs.etfs.data.model.struct.FileContent.serializePluginCustomData
        // If any changes are made to the size of the output (outside the PCD), add an offset for that size there!
        callback.onStartWritePluginCustomDataSection(MetadataPosition.AFTER_FILE_CONTENT, (short) customData.size());
        pluginData = generateWritingPluginCustomDataList(context);
        pluginDataLength = (short) pluginData.size();
        writeAndCheck(toBytes(pluginDataLength), os);

        for (PluginCustomData pcd : customData) {
            os = pcd.serialize(os, isMultitapePart, context, callback);
        }
        callback.onEndWritePluginCustomDataSection(MetadataPosition.AFTER_FILE_CONTENT);
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        // Step 1: Recalculate dynamic values

        // Step 2: Serialize
        callback.onStartWriteFile(fileEntryData.getTargetFile(), fileEntryData.getFileSize());
        // Copies file. Try-Catch with resources makes sure that the InputStream is closed properly
        long numTransferredBytes = 0;
        long lastReportedPosition = 0;
        // Write file content to tape

        try (InputStream is = Files.newInputStream(fileEntryData.getTargetFile())) {
            long currentFileSize = Files.size(fileEntryData.getTargetFile());

            if (currentFileSize != fileEntryData.getFileSize()) {
                throw new IOException("File size has changed. Refusing to write.");
            }

            byte[] buffer = new byte[getPreferredReadBufferSize()];

            int numBytesRead;
            while ((numBytesRead = is.read(buffer)) > 0) {
                try {
                    if (numBytesRead == buffer.length) {
                        writeAndCheckAndRequestTapeChange(buffer, os, callback, context, getFileEntryData());
                    } else {
                        writeAndCheckAndRequestTapeChange(buffer, 0, numBytesRead, os, callback, context, getFileEntryData());
                    }
                } catch (IOException e) {
                    // Encapsulate exception to rethrow it later, as wrie exceptions cannot be handeled with a consistent state
                    throw new EtfsException("Error while writing file data to tape", e, EtfsException.ErrorCode.MISC_ERROR);
                }

                numTransferredBytes += numBytesRead;
                context.getCurrentPhysicalTapeInfo().setAmountOfFileBytesWritten(context.getCurrentPhysicalTapeInfo().getAmountOfFileBytesWritten() + numBytesRead);

                if (numTransferredBytes - lastReportedPosition >= callback.getReportFileWriteProgressBytes()) {
                    callback.onFileProgress(fileEntryData.getTargetFile(), numTransferredBytes, fileEntryData.getFileSize());
                    lastReportedPosition = numTransferredBytes;
                }

                // TODO: [Low] Rework where overall number of bytes written is stored and try to cache the total amount
                if (context.getCurrentPhysicalTapeInfo().getAmountOfFileBytesWritten() - context.getCurrentPhysicalTapeInfo().getLastReportedFileBytesWritten() >= callback.getReportOverallFileWriteProgressBytes()) {
                    callback.onOverallFileProgress(context.getCurrentPhysicalTapeInfo().getAmountOfFileBytesWritten(), context.getCurrentPhysicalTapeInfo().getTableOfContentSection().getFileEntries().stream().map(FileEntry::getFileSize).reduce(Long::sum).get());
                    context.getCurrentPhysicalTapeInfo().setLastReportedFileBytesWritten(context.getCurrentPhysicalTapeInfo().getAmountOfFileBytesWritten());
                }
            }
        } catch (IOException e) {
            if (e instanceof EtfsException && "Error while writing file data to tape".equals(e.getMessage())) {
                throw e;
            }

            LOGGER.error("Error while writing file "+fileEntryData.getTargetFile().toString()+" to tape. Rest of file will be skipped and file will be marked with READ_ERROR", e);
            runtimeMeta.setFlag(FileRuntimeMetaDataField.RUNTIME_METADATA_FLAG.FILE_NOT_READABLE);
            callback.onUnreadableFile(fileEntryData.getTargetFile(), e);
        } finally {
            // Write missing byte, as else the reading side would overread
            writePaddingData(os, fileEntryData.getFileSize() - numTransferredBytes);
        }

        writeAndCheck(toBytes(runtimeMeta.getRuntimeMetadata()), os);
        callback.onEndWriteFile(fileEntryData.getTargetFile(), fileEntryData.getFileSize());

        serializePluginCustomData(os, pluginData, isMultitapePart, context, callback);

        return os;
    }

    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        Path target = fileEntryData.getTargetFile();

        callback.onStartReadFile(target, fileEntryData.getFileSize());

        // TODO: [Medium] Put this before file writing to fail faster and create directories beforehand
        if (Files.exists(target.getParent()) && !Files.isDirectory(target.getParent())) {
            throw new IOException("Could not create/use directory for file "+target+", as "+target.getParent()+" already exists, but is not a directory");
        }

        if (!Files.exists(target.getParent())) {
            Files.createDirectories(target.getParent());
        }

        // TODO: [Low] Try to move to callable interface to make operation more customizable
        byte[] buffer = new byte[getPreferredReadBufferSize()];
        long numBytesRemaining = fileEntryData.getFileSize();
        long bytesSinceLastReport = 0;

        try (OutputStream os = Files.newOutputStream(target)) {
            while (numBytesRemaining > 0) {
                int numBytesRead;
                if (numBytesRemaining >= buffer.length) {
                    numBytesRead = is.read(buffer);
                } else {
                    buffer = new byte[(int) numBytesRemaining];
                    numBytesRead = is.read(buffer);
                }

                if (numBytesRead != buffer.length) {
                    // End of tape reached, switch tape
                    try {
                        context.switchMutitapeTape(is, callback);

                        // Tape correctly switched, switch input stream
                        is = context.getOpDispatcher().getCurrentInputStream();
                    } catch (EtfsException e) {
                        if (e.getErrorCode() == EtfsException.ErrorCode.MULTITAPE_NOT_NEXT_TAPE) {
                            // User forces us to use a different tape. Abort writing this file
                            callback.onEndReadFile(target, (fileEntryData.getFileSize() - numBytesRemaining));

                            // Return new InputStream from switched tape
                            return context.getOpDispatcher().getCurrentInputStream();
                        } else {
                            // Other error, not handable here
                            throw e;
                        }
                    }
                }

                numBytesRemaining -= numBytesRead;
                bytesSinceLastReport += numBytesRead;

                if (fileEntryData.isRestore()) {
                    os.write(buffer, 0, numBytesRead);

                    if (bytesSinceLastReport >= callback.getReportFileWriteProgressBytes()) {
                        callback.onFileProgress(target, fileEntryData.getFileSize() - numBytesRemaining, fileEntryData.getFileSize());
                        bytesSinceLastReport = 0;
                    }
                }
            }
        }

        callback.onEndReadFile(target, fileEntryData.getFileSize());
        runtimeMeta = new FileRuntimeMetaDataField(readNextInt(is));
        pluginDataLength = readNextShort(is);
        for (short i = 0; i < pluginDataLength; i++) {
            PluginCustomData pcd = new PluginCustomData(MetadataPosition.AFTER_FILE_CONTENT, this, getFileEntryData().getTargetFile());
            pluginData.add(pcd);
            is = pcd.deserialize(is, version, ignorePluginSections, context, callback);
            // TODO: [Blocker] Plugin execution?
        }

        return is;
    }
}
