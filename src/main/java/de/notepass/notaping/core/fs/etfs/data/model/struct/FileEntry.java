package de.notepass.notaping.core.fs.etfs.data.model.struct;

import de.notepass.notaping.core.fs.etfs.data.listener.impl.EtfsNoopWriteProgressListener;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.model.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.metadata.FileMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import de.notepass.notaping.core.shared.ByteCountingOutputStream;
import de.notepass.notaping.core.shared.NoopOutputStream;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Java class representation of the "FileEntry" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class FileEntry implements EtfsSerializableElement {
    // overall_file_content_entry_site: 8 int: Size of file entry in the file section in bytes (used for skipping files in selective restore)
    private long overallFileContentEntrySize;

    // file_size: 8 int: Size of the file in bytes
    private long fileSize;

    // file_created: 8 int: Unix timestamp of file creation datetime
    private long fileCreated;

    // file_modified: 8 int: Unix timestamp of when the file was last modified
    private long fileModified;

    // filename_size: 2 int: Size of the filename field
    private short filenameSize;

    // filename: 0:32767 string: Path and name of the file saved (normalized, see WIKI)
    private String filename;

    // file_meta: 8 int: Additional file meta data (See Metadata section for more info)
    private FileMetaDataField fileMeta = new FileMetaDataField();

    // plugin_data_length: 2 int: Number of entries in the plugin_data list
    private short pluginDataLength;

    // plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_DATA)
    private List<PluginCustomData> pluginData = new ArrayList<>();

    // This data isn't written to the tape, it's just here to help
    private Path targetFile;
    private boolean restore;
    private FileContent fileContent;

    /**
     * filename_size: 2 int: Size of the filename field
     *
     * @return
     */
    public short getFilenameSize() {
        return filenameSize;
    }

    /**
     * filename_size: 2 int: Size of the filename field
     *
     * @param filenameSize
     */
    public void setFilenameSize(short filenameSize) {
        this.filenameSize = filenameSize;
    }

    /**
     * filename: 0:32767 string: Path and name of the file saved (normalized, see WIKI)
     *
     * @return
     */
    public String getFilename() {
        return filename;
    }

    /**
     * filename: 0:32767 string: Path and name of the file saved (normalized, see WIKI)
     *
     * @param filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * file_meta: 8 int: Additional file meta data (See Metadata section for more info)
     *
     * @return
     */
    public FileMetaDataField getFileMeta() {
        return fileMeta;
    }

    /**
     * file_meta: 8 int: Additional file meta data (See Metadata section for more info)
     *
     * @param fileMeta
     */
    public void setFileMeta(FileMetaDataField fileMeta) {
        this.fileMeta = fileMeta;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     *
     * @return
     */
    public short getPluginDataLength() {
        return pluginDataLength;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     *
     * @param pluginDataLength
     */
    public void setPluginDataLength(short pluginDataLength) {
        this.pluginDataLength = pluginDataLength;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_DATA)
     *
     * @return
     */
    public List<PluginCustomData> getPluginData() {
        return pluginData;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_DATA)
     *
     * @param pluginData
     */
    public void setPluginData(List<PluginCustomData> pluginData) {
        this.pluginData = pluginData;
    }

    /**
     * Reference to file on disk the content should be written to/read from
     *
     * @return
     */
    public Path getTargetFile() {
        return targetFile;
    }

    /**
     * Reference to file on disk the content should be written to/read from
     *
     * @return
     */
    public void setTargetFile(Path targetFile) {
        this.targetFile = targetFile;
    }

    /**
     * Indicates if the given file should be restored
     * @return
     */
    public boolean isRestore() {
        return restore;
    }

    /**
     * file_size: 8 int: Size of file in bytes
     * @return
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * file_size: 8 int: Size of file in bytes
     * @param fileSize
     */
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * file_created: 8 int: Unix timestamp of file creation datetime
     * @return
     */
    public long getFileCreated() {
        return fileCreated;
    }

    /**
     * file_created: 8 int: Unix timestamp of file creation datetime
     * @param fileCreated
     */
    public void setFileCreated(long fileCreated) {
        this.fileCreated = fileCreated;
    }

    /**
     * file_modified: 8 int: Unix timestamp of when the file was last modified
     * @return
     */
    public long getFileModified() {
        return fileModified;
    }

    /**
     * file_modified: 8 int: Unix timestamp of when the file was last modified
     * @param fileModified
     */
    public void setFileModified(long fileModified) {
        this.fileModified = fileModified;
    }

    /**
     * Indicates if the given file should be restored
     * @param restore
     */
    public void setRestore(boolean restore) {
        this.restore = restore;
    }

    /**
     * overall_file_content_entry_site: 8 int: Size of file entry in the file section in bytes (used for skipping files in selective restore)
     * @return
     */
    public long getOverallFileContentEntrySize() {
        return overallFileContentEntrySize;
    }

    /**
     * overall_file_content_entry_site: 8 int: Size of file entry in the file section in bytes (used for skipping files in selective restore)
     * @param overallFileContentEntrySize
     */
    public void setOverallFileContentEntrySize(long overallFileContentEntrySize) {
        this.overallFileContentEntrySize = overallFileContentEntrySize;
    }

    public FileContent getFileContent() {
        return fileContent;
    }

    public void setFileContent(FileContent fileContent) {
        this.fileContent = fileContent;
    }

    @Override
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        callback.onStartWriteTableOfContentsEntry(this);

        // Step 1: Recalculate fields
        filename = normalizeFilePath(targetFile);
        filenameSize = (short) sizeof(filename);
        try {
            BasicFileAttributes fileAttributes = Files.readAttributes(targetFile, BasicFileAttributes.class);
            fileSize = Files.size(targetFile);
            fileCreated = fileAttributes.creationTime().toMillis();
            fileModified = fileAttributes.lastModifiedTime().toMillis();
        } catch (Exception e) {
            fileMeta.setFlag(FileMetaDataField.FILE_METADATA_FLAG.IS_NOT_WRITTEN);
            fileSize = 0;
        }

        // Calculate plugin sizes for files
        // TODO: [High] Call standard callback here?
        ByteCountingOutputStream bcos = new ByteCountingOutputStream();
        List<PluginCustomData> contentCustomData = fileContent.generateWritingPluginCustomDataList(context);
        fileContent.serializePluginCustomData(bcos, contentCustomData, isMultitapePart, context, new EtfsNoopWriteProgressListener());
        // Make sure to remove the size of the short written for pluginDataLength
        overallFileContentEntrySize = FileContent.BASE_SIZE + fileSize + (bcos.getBytesWritten() - Short.BYTES);

        // Step 2: Write data
        writeAndCheck(toBytes(filenameSize), os);
        writeAndCheck(toBytes(filename), os);
        writeAndCheck(toBytes(overallFileContentEntrySize), os);
        writeAndCheck(toBytes(fileSize), os);
        writeAndCheck(toBytes(fileCreated), os);
        writeAndCheck(toBytes(fileModified), os);
        writeAndCheck(toBytes(fileMeta.getFileMetadata()), os);
        if (!isMultitapePart) {
            // Only create local custom data entry references on the first tape call
            for (EtfsPlugin plugin : context.getPluginRegistry().getActivePlugins()) {
                if (plugin.wantsToWritePluginData(MetadataPosition.AFTER_TOC_ENTRY, this)) {
                    // Only write data the plugin wants to write and if this is the first header
                    PluginCustomData pcd = new PluginCustomData(MetadataPosition.AFTER_TOC_ENTRY, this);
                    pluginData.add(pcd);
                }
            }
        }
        pluginDataLength = (short) pluginData.size();
        writeAndCheck(toBytes(pluginDataLength), os);
        callback.onStartWritePluginCustomDataSection(MetadataPosition.AFTER_TOC_ENTRY, pluginDataLength);
        for (PluginCustomData pcd : pluginData) {
            os = pcd.serialize(os, isMultitapePart, context, callback);
        }
        callback.onEndWritePluginCustomDataSection(MetadataPosition.AFTER_TOC_ENTRY);
        callback.onEndWriteTableOfContentsEntry(this);

        return os;
    }

    @Override
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        callback.onStartReadTableOfContentsEntry();

        filenameSize = readNextShort(is);
        filename = readNextString(is, filenameSize);
        overallFileContentEntrySize = readNextLong(is);
        fileSize = readNextLong(is);
        fileCreated = readNextLong(is);
        fileModified = readNextLong(is);
        fileMeta = new FileMetaDataField(readNextLong(is));
        pluginDataLength = readNextShort(is);

        // Moved to TOC parsing, as the path remapper needs to be prepared first, which can
        // only be done after the FEs are parsed
        //targetFile = context.getPathRemapper().map(filename);

        callback.onStartReadPluginCustomDataSection(MetadataPosition.AFTER_TOC_ENTRY, pluginDataLength);
        for (short i = 0; i < pluginDataLength; i++) {
            PluginCustomData pcd = new PluginCustomData(MetadataPosition.AFTER_TOC_ENTRY, this);
            pluginData.add(pcd);
            is = pcd.deserialize(is, version, ignorePluginSections, context, callback);
        }
        callback.onEndReadPluginCustomDataSection(MetadataPosition.AFTER_TOC_ENTRY);
        callback.onEndReadTableOfContentsEntry(this);

        return is;
    }
}
