package de.notepass.notaping.core.fs.etfs.data.listener.model;

/**
 * Indicates how the user wants to react to a multitape request (e.g. tape change or wrong tape inserted)
 */
public enum MultitapeAction {
    /**
     * Indicates that the tape has been changed and should be reread to reevaluate the current situation
     */
    TAPE_CHANGED,

    /**
     * Indicates that the program should continue with the current tape, if possible
     */
    CONTINUE,

    /**
     * Indicates that the current action should be canceled
     */
    ABORT
}
