package de.notepass.notaping.core.fs.etfs.action.model;

import de.notepass.notaping.core.fs.etfs.data.metadata.HeaderMetaDataField;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface TapeSpecificOperationDispatcher {
    public HeaderMetaDataField.HEADER_METADATA_FLAG[] getCapabilities();
    public boolean isEndOfTapeException(Exception e);
    public InputStream getCurrentInputStream() throws IOException;
    public WrittenBytesAwareOutputStream getCurrentOutputStream() throws IOException;

    /**
     * Writes the given number of EOF marks at the current ape position. Please make sure that all output buffers are
     * flushed beforehand so that it appears at the correct position. Please also note, that markers should only be
     * written after an entire block
     * @param amount Number of filemarks to write
     */
    public void writeFilemarks(int amount) throws IOException;

    /**
     * Sets the position of the tape in a way, that the next block read contains the given byte index
     * @param byteIndex The byte to read as offset from 0 (e.g. if the 2000 byte on the tape should be read, pass 2000)
     * @return The amount of bytes that need to be read before the given byte is read (As tapes can only be read in a manner
     * of complete blocks)
     */
    public int setBytePosition(long byteIndex) throws IOException;

    /**
     * Returns an output stream used to write headers for the next instance in a multitape volume.
     * This allows for ignoring the main output stream, which may have buffered data and writing with a stream that
     * will be discarded later on
     * @return A temporary stream
     * @throws IOException
     */
    public WrittenBytesAwareOutputStream getTemporaryHeaderOutputStream() throws IOException;

    /**
     * Removes the stream created by {@link #getTemporaryHeaderOutputStream()}
     * @return A temporary stream
     * @throws IOException
     */
    public void discardTemporaryHeaderOutputStream(WrittenBytesAwareOutputStream os) throws IOException;
}
