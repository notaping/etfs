package de.notepass.notaping.core.fs.etfs.data.runtime;

import de.notepass.notaping.core.fs.etfs.action.impl.NoopTapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.action.model.TapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.data.config.EtfsCoreConfiguration;
import de.notepass.notaping.core.fs.etfs.data.model.section.FileSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileContent;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.data.model.struct.PluginInfo;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.PluginRegistry;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.*;

public class EtfsWriteDirectionBuilder {
    private List<Path> filesToWrite = new LinkedList<>();
    private List<EtfsPlugin> plugins = new LinkedList<>();
    private String label;
    private TapeSpecificOperationDispatcher opDispatch;
    private static final Logger LOGGER = LoggerFactory.getLogger(EtfsWriteDirectionBuilder.class);

    /**
     * Creates a new instance of the factory and passes the required data
     * @return
     */
    public static EtfsWriteDirectionBuilder newInstance() {
        return new EtfsWriteDirectionBuilder();
    }

    public EtfsWriteDirectionBuilder addFile(Path p) {
        filesToWrite.add(p);
        return this;
    }

    public EtfsWriteDirectionBuilder addFiles(Path ... files) {
        Collections.addAll(filesToWrite, files);
        return this;
    }

    public EtfsWriteDirectionBuilder addFiles(Collection<Path> files) {
        filesToWrite.addAll(files);
        return this;
    }

    public EtfsWriteDirectionBuilder addPlugin(EtfsPlugin plugin) {
        plugins.add(plugin);
        return this;
    }

    public EtfsWriteDirectionBuilder addPlugins(EtfsPlugin ... plugin) {
        Collections.addAll(plugins, plugin);
        return this;
    }

    public EtfsWriteDirectionBuilder withLabel(String label) {
        this.label = label;
        return this;
    }

    public EtfsWriteDirectionBuilder withTapeSpecificOperationDispatcher(TapeSpecificOperationDispatcher opd) {
        opDispatch = opd;
        return this;
    }

    public EtfsInstance build() {
        EtfsInstance instance = new EtfsInstance();
        instance.setPluginRegistry(new PluginRegistry());

        // ======================= Header Part =======================
        HeaderSection header = new HeaderSection();
        if (label == null) {
            // Make sure that the label is not null
            label = "";
        }
        header.setLabel(label);

        // Create a volume ID
        header.setVolumeId(UUID.randomUUID());

        header.setVersion(EtfsCoreConfiguration.BINARY_VERSION);
        header.setMinVersion(EtfsCoreConfiguration.MIN_BINARY_COMPAT_VERSION);

        // Set index to 0, as this is the first tape
        header.setMultitapeIndex((short) 0);

        // TODO: [Low] Currently always true
        header.setAllowMultitape(true);

        if (opDispatch == null) {
            // If not OPD is given, fall back to noop (Removes EOF functionality)
            LOGGER.warn("TapeSpecificOperationDispatcher is null. Falling back to NOOP implementation.");
            opDispatch = new NoopTapeSpecificOperationDispatcher();
        }

        instance.setOpDispatcher(opDispatch);

        // ======================= Plugin config Part =======================
        PluginConfigSection pcs = new PluginConfigSection();
        List<PluginInfo> pluginInfos = new ArrayList<>(plugins.size() + 10);

        short remappedId = 0;
        for (EtfsPlugin plugin : plugins) {
            instance.getPluginRegistry().register(plugin);
            PluginInfo pi = new PluginInfo();
            pi.setId(plugin.getID());
            pi.setVersion(plugin.getVersion());
            pi.setInternalId(remappedId);
            pluginInfos.add(pi);

            remappedId++;
        }

        pcs.setPluginInfo(pluginInfos);

        instance.getPluginRegistry().buildShortIdCache(pcs);

        // ======================= TOC Part =======================
        TableOfContentSection tocSection = new TableOfContentSection();
        List<FileEntry> fileEntries = new ArrayList<>(filesToWrite.size() + 10);
        for (Path p : filesToWrite) {
            FileEntry fileEntry = new FileEntry();
            fileEntry.setTargetFile(p);
            fileEntry.setFilename(SerializeUtils.normalizeFilePath(p));
            fileEntries.add(fileEntry);
        }

        tocSection.setFileEntries(fileEntries);

        // ======================= Files Part =======================
        FileSection fileSection = new FileSection();
        List<FileContent> fileContents = new ArrayList<>(fileEntries.size() + 10);
        for (FileEntry fileEntry : fileEntries) {
            FileContent fileContent = new FileContent();
            fileContent.setFileEntryData(fileEntry);
            fileEntry.setFileContent(fileContent);
            fileContents.add(fileContent);
        }

        fileSection.setFiles(fileContents);

        // ======================= Fill container =======================
        instance.setTapeIndex(header.getMultitapeIndex());
        instance.getLogicalTapeInfo().setHeaderSection(header);
        instance.getLogicalTapeInfo().setPluginConfigSection(pcs);
        instance.getLogicalTapeInfo().setTableOfContentSection(tocSection);
        instance.getLogicalTapeInfo().setFileSection(fileSection);

        // Return result
        return instance;
    }
}
