package de.notepass.notaping.core.fs.etfs.action.model;

import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;

import java.util.List;

public interface FileSelector {
    public void prepareFileSelection(List<FileEntry> allFiles);
    public boolean writeFile(FileEntry file);
}
