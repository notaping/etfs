package de.notepass.notaping.core.fs.etfs.util;

import de.notepass.notaping.core.fs.etfs.EtfsException;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;

public class SerializeUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(SerializeUtils.class);

    /**
     * Returns size in bytes of an integer
     * @param i
     * @return
     */
    public static int sizeof(int i) {
        return Integer.BYTES;
    }

    /**
     * Returns size in bytes of a long
     * @param i
     * @return
     */
    public static int sizeof(long i) {
        return Long.BYTES;
    }

    /**
     * Returns size in bytes of a short
     * @param i
     * @return
     */
    public static int sizeof(short i) {
        return Short.BYTES;
    }

    /**
     * Returns size in bytes of a byte
     * @param i
     * @return
     */
    public static int sizeof(byte i) {
        return Byte.BYTES;
    }

    /**
     * Returns the size of the UTF-8 encoded version of the given string
     * @param i String to calculate the size of
     * @return
     */
    public static int sizeof(String i) {
        return i.getBytes(StandardCharsets.UTF_8).length;
    }

    /**
     * Reads a short from the input stream and returns it
     * @param is InputStream to read from
     * @return next integer in the stream
     * @throws IOException On stream error
     */
    public static short readNextShort(InputStream is) throws IOException {
        byte[] buffer = new byte[2];
        is.read(buffer);
        return shortFromBytes(buffer);
    }

    /**
     * Assembled a byte array into a short
     * @param bytes Bytes to assemble
     * @return Assembled short
     */
    public static short shortFromBytes(byte[] bytes) {
        return shortFromBytes(bytes, 0);
    }

    /**
     * Assembled a byte array into a short
     * @param bytes Bytes to assemble
     * @return Assembled short
     */
    public static short shortFromBytes(byte[] bytes, int offset) {
        return (short) (((short) bytes[1 + offset] << 8)
                | ((short) bytes[offset] & 0xff));
    }

    /**
     * Reads a byte from the input stream and returns it
     * @param is InputStream to read from
     * @return next byte in the stream
     * @throws IOException On stream error
     */
    public static byte readNextByte(InputStream is) throws IOException {
        byte[] buffer = new byte[1];
        is.read(buffer);
        return buffer[0];
    }

    /**
     * Reads an integer from the input stream and returns it
     * @param is InputStream to read from
     * @return next integer in the stream
     * @throws IOException On stream error
     */
    public static int readNextInt(InputStream is) throws IOException {
        byte[] buffer = new byte[4];
        is.read(buffer);
        return intFromBytes(buffer);
    }

    /**
     * Assembled a byte array into an int
     * @param bytes Bytes to assemble
     * @return Assembled int
     */
    public static int intFromBytes(byte[] bytes) {
        return intFromBytes(bytes, 0);
    }

    /**
     * Assembled a byte array into an int
     * @param bytes Bytes to assemble
     * @return Assembled int
     */
    public static int intFromBytes(byte[] bytes, int offset) {
        return ((int) bytes[3 + offset] << 24)
                | ((int) bytes[2 + offset] & 0xff) << 16
                | ((int) bytes[1 + offset] & 0xff) << 8
                | ((int) bytes[offset] & 0xff);

    }

    /**
     * Reads a long from the input stream and returns it
     * @param is InputStream to read from
     * @return next long in the stream
     * @throws IOException On stream error
     */
    public static long readNextLong(InputStream is) throws IOException {
        byte[] buffer = new byte[8];
        is.read(buffer);
        return longFromBytes(buffer);
    }

    /**
     * Assembled a byte array into a long
     * @param bytes Bytes to assemble
     * @return Assembled long
     */
    public static long longFromBytes(byte[] bytes) {
        return longFromBytes(bytes, 0);
    }

    /**
     * Assembled a byte array into a long
     * @param bytes Bytes to assemble
     * @return Assembled long
     */
    public static long longFromBytes(byte[] bytes, int offset) {
        return ((long) bytes[7 + offset] << 56)
                | ((long) bytes[6 + offset] & 0xff) << 48
                | ((long) bytes[5 + offset] & 0xff) << 40
                | ((long) bytes[4 + offset] & 0xff) << 32
                | ((long) bytes[3 + offset] & 0xff) << 24
                | ((long) bytes[2 + offset] & 0xff) << 16
                | ((long) bytes[1 + offset] & 0xff) << 8
                | ((long) bytes[offset] & 0xff);

    }

    /**
     * Reads a string from the input stream and returns it
     * @param is InputStream to read from
     * @param length Size of the string to read
     * @return next string in the stream
     * @throws IOException On stream error
     */
    public static String readNextString(InputStream is, int length) throws IOException {
        byte[] buffer = new byte[length];
        is.read(buffer);
        return stringFromBytes(buffer);
    }

    /**
     * Assembled a byte array into a string
     * @param bytes Bytes to assemble
     * @return Assembled strin
     */
    public static String stringFromBytes(byte[] bytes) {
        return new String(bytes, StandardCharsets.UTF_8);
    }

    /**
     * Assembled a byte array into a string
     * @param bytes Bytes to assemble
     * @return Assembled strin
     */
    public static String stringFromBytes(byte[] bytes, int offset, int length) {
        return new String(bytes, offset, length, StandardCharsets.UTF_8);
    }

    /**
     * Disassembled a short into a byte array
     * @param s Short to disassemble
     * @return Disassembled short
     */
    public static byte[] toBytes(short s) {
        return new byte[]{
                (byte) s,
                (byte) (s >> 8)
        };
    }

    /**
     * Disassembled an int into a byte array
     * @param i Int to disassemble
     * @return Disassembled int
     */
    public static byte[] toBytes(int i) {
        return new byte[]{
                (byte) i,
                (byte) (i >> 8),
                (byte) (i >> 16),
                (byte) (i >> 24)
        };
    }

    /**
     * Disassembled a long into a byte array
     * @param l Long to disassemble
     * @return Disassembled short
     */
    public static byte[] toBytes(long l) {
        return new byte[]{
                (byte) l,
                (byte) (l >> 8),
                (byte) (l >> 16),
                (byte) (l >> 24),
                (byte) (l >> 32),
                (byte) (l >> 40),
                (byte) (l >> 48),
                (byte) (l >> 56)
        };
    }

    /**
     * Disassembled a byte into a byte array
     * @param b Byte to disassemble
     * @return Disassembled byte
     */
    public static byte[] toBytes(byte b) {
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES);
        bb.put(b);
        return bb.array();
    }

    /**
     * Disassembled a boolean into a byte array
     * @param bool Boolean to disassemble
     * @return Disassembled boolean
     */
    public static byte[] toBytes(boolean bool) {
        byte b = 0;
        if (bool) {
            b = 1;
        }

        return toBytes(b);
    }

    /**
     * Disassembled a string into an UTF-8 encoded byte array
     * @param s String to disassemble
     * @return Disassembled string
     */
    public static byte[] toBytes(String s) {
        return s.getBytes(StandardCharsets.UTF_8);
    }

    public static Path getTempFile() throws IOException {
        return createTempFile("ETFS-TEMPDATA-", ".tmp");
    }

    /**
     * Normalizes the file path for saving on tape
     * @param p
     * @return
     */
    public static String normalizeFilePath(Path p) {
        return p.normalize().toString();
    }

    /**
     * Writes the given amount of 0 bytes to the output stream
     * @param os
     * @param numBytes
     * @throws IOException
     */
    public static void writePaddingData(OutputStream os, long numBytes) throws IOException {
        long numRemainingBytes = numBytes;
        byte[] buffer = new byte[SerializeUtils.getPreferredReadBufferSize()];
        while (numRemainingBytes > 0) {
            if (buffer.length <= numRemainingBytes) {
                // Write 0 buffer. Compression will be happy :)
                os.write(buffer);
                numRemainingBytes -= buffer.length;
            } else {
                os.write(buffer, 0, (int) numRemainingBytes);
                numRemainingBytes = 0;
            }
        }
    }

    /**
     * Returns the preferred buffer size for reading files
     * @return
     */
    public static int getPreferredReadBufferSize() {
        int defaultValue = 4096;
        int calculatedValue = 0;

        //TODO: [Blocker] Document
        String size = System.getProperty("etfs.core.buffer.read.size");
        if (size != null && !size.isEmpty()) {
            try {
                calculatedValue = Integer.parseInt(size);
            } catch (Exception e) {
                LOGGER.warn("Could not parse java property etfs.core.buffer.read.size. Given value is: "+size+". Will" +
                        "default to "+defaultValue);
            }
        }

        if (calculatedValue < 64) {
            if (size != null && !size.isEmpty()) {
                LOGGER.debug("System-property etfs.core.buffer.read.size is invalid. Property is: "+size+", parsed value" +
                        "is: "+calculatedValue+". Value must be >=64. Will use default value of "+defaultValue);
            }
            return defaultValue;
        } else {
            return calculatedValue;
        }
    }

    /**
     * Returns the preferred size of the buffer for writing data to the tape in bytes
     * @return
     */
    public static int getPreferredTapeOutputBufferSize(int blockSizeBytes) {
        int size = 0;
        try {
            // TODO: [Blocker] Document
            size = Integer.parseInt(System.getProperty("etfs.core.buffer.write.tape.size"));
        } catch (Exception e) {
            // Set to hardcoded default (512 MB)
            size = 512 * 1024 * 1024;
        }

        // Align the buffer size with the block size
        size -= size % blockSizeBytes;

        assert size % blockSizeBytes == 0;

        return size;
    }

    /**
     * Writes the given bytes into the given output stream and checks if all bytes where written
     * @param bytes bytes to write
     * @param os stream to write to
     * @throws IOException On stream error or if not all bytes where written
     */
    public static void writeAndCheck(byte[] bytes, WrittenBytesAwareOutputStream os) throws IOException {
        int written = os.writeA(bytes);

        assureAllWritten(written, bytes.length);
    }

    /**
     * Writes the given bytes into the given output stream and checks if all bytes where written
     * @param bytes bytes to write
     * @param os stream to write to
     * @param amount Number of bytes to write starting at offset
     * @param offset index of first byte to write
     * @throws IOException On stream error or if not all bytes where written
     */
    public static void writeAndCheck(byte[] bytes, int offset, int amount, WrittenBytesAwareOutputStream os) throws IOException {
        int written = os.writeA(bytes, offset, amount);

        assureAllWritten(written, amount);
    }

    /**
     * Checks if the given amount of bytes was written and throws an exception if not
     */
    private static void assureAllWritten(int is, int should) throws IOException {
        if (should != is) {
            throw new IOException("No space left on target device. Cannot apply multitape at this point.");
        }
    }

    /**
     * Writes the given bytes into the given output stream and checks if all bytes where written. If bytes can't be written,
     * a new tape is requested via the de.notepass.notaping.test.callback
     * @param bytes bytes to write
     * @param os stream to write to
     * @param amount Number of bytes to write starting at offset
     * @param offset index of first byte to write
     * @param callback Callback to use to request a new tape
     * @param context Instance this call belongs to
     * @throws IOException On stream error or if not all bytes where written
     */
    public static void writeAndCheckAndRequestTapeChange(byte[] bytes, int offset, int amount, WrittenBytesAwareOutputStream os, EtfsWriteProgressCallback callback, EtfsInstance context, FileEntry currentFileEntry) throws IOException {
        int written = os.writeA(bytes, offset, amount);

        ensureAllWrittenAndRequestTape(bytes, offset, written, amount, os, callback, context, currentFileEntry);
    }

    /**
     * Writes the given bytes into the given output stream and checks if all bytes where written. If bytes can't be written,
     * a new tape is requested via the de.notepass.notaping.test.callback
     * @param bytes bytes to write
     * @param os stream to write to
     * @param callback Callback to use to request a new tape
     * @param context Instance this call belongs to
     * @throws IOException On stream error or if not all bytes where written
     */
    public static void writeAndCheckAndRequestTapeChange(byte[] bytes, WrittenBytesAwareOutputStream os, EtfsWriteProgressCallback callback, EtfsInstance context, FileEntry currentFileEntry) throws IOException {
        writeAndCheckAndRequestTapeChange(bytes, 0, bytes.length, os, callback, context, currentFileEntry);
    }

    public static void flushAndCheckAndRequestTapeChange(WrittenBytesAwareOutputStream os, EtfsWriteProgressCallback callback, EtfsInstance context, FileEntry lastWrittenFileEntry) throws IOException {
        int expectedFlushSize = os.getExpectedNextFlushSize();
        int actualFlushSize = os.flushA();

        if (actualFlushSize < expectedFlushSize) {
            doTapeSwitchRequest(callback, context, () -> {
                context.writeNextTapeHeader(lastWrittenFileEntry, callback);
                flushAndCheckAndRequestTapeChange(os, callback, context, lastWrittenFileEntry);
            });
        }
    }

    private static void doTapeSwitchRequest(EtfsWriteProgressCallback callback, EtfsInstance context, TapeSwitchRunnable onSuccess) throws IOException {
        MultitapeAction action = callback.multitapeTapeChangeNeeded(context.getLogicalTapeInfo().getHeaderSection().getLabel(), context.getLogicalTapeInfo().getHeaderSection().getVolumeId(), context.getTapeIndex(), context.getTapeIndex() + 1);

        switch (action) {
            case CONTINUE:
            default:
                throw new EtfsException("Invalid operation "+action+" for tape change request", EtfsException.ErrorCode.MISC_ERROR);
            case ABORT:
                throw new EtfsException("User aborted multitape creation", EtfsException.ErrorCode.USER_ABORT);
            case TAPE_CHANGED:
                LOGGER.debug("User changed tape for writing (From "+context.getTapeIndex()+" to "+(context.getTapeIndex() + 1));
                onSuccess.run();
                break;
        }
    }

    /**
     * Checks if the given amount of bytes was written and requests a new tape if not
     */
    private static void ensureAllWrittenAndRequestTape(byte[] bytes, int offset, int is, int should, WrittenBytesAwareOutputStream os, EtfsWriteProgressCallback callback, EtfsInstance context, FileEntry currentFileEntry) throws IOException {
        if (is != should) {
            doTapeSwitchRequest(callback, context, () -> {
                context.writeNextTapeHeader(currentFileEntry, callback);
                // Write remaining data
                int bytesWritten = os.writeA(bytes, offset + is, should - is);
                // Ensure it was written
                // TODO: [Low] Can this be done without recursion?
                ensureAllWrittenAndRequestTape(bytes, offset, bytesWritten + is, should, os, callback, context, currentFileEntry);
            });
        }
    }

    public static Path createTempFile(String prefix, String suffix, FileAttribute<?> ... attributes) throws IOException {
        //TODO: [Low] Make customizable via system property
        Path basedir = Path.of(System.getProperty("java.io.tmpdir"));
        Path tempFile = Files.createTempFile(basedir, prefix, suffix, attributes);
        tempFile.toFile().deleteOnExit();
        return tempFile;
    }

    private interface TapeSwitchRunnable {
        public void run() throws IOException;
    }

    /**
     * Calculates the amount of bytes to skip from the start of the tape to skip the given file. The result
     * of this function will return the first byte which is to be read to read the NEXT file after this
     * @param context
     * @param file
     * @return
     */
    public static long calculateByteOffset(EtfsInstance context, FileEntry file) {
        long bytes = context.getLogicalTapeInfo().getFileSection().getHeaderSizeBytes();
        for (FileEntry fileEntry : context.getLogicalTapeInfo().getTableOfContentSection().getFileEntries()) {
            bytes += fileEntry.getFileSize();

            if (file == fileEntry) {
                break;
            }
        }

        return bytes;
    }
}
