package de.notepass.notaping.core.fs.etfs.data.model;

import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsWriteProgressCallback;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/**
 * Interface for ETFS-specific (de)-serialize operations
 */
public interface EtfsSerializableElement extends Serializable {
    /**
     * Serializes an element into the given output stream
     * @param os outputStream to write to
     * @param isMultitapePart Indicates that this write request happened on the second to n tape in a multitape volume,
     *                        thus skipping plugin config and TOC plugin data evaluation in the header part
     * @param context etfs instance as context for global methods
     * @param callback Callback given by the user
     * @return New OutputStream to use (Is changed when changing tapes)
     * @throws IOException
     */
    public WrittenBytesAwareOutputStream serialize(WrittenBytesAwareOutputStream os, boolean isMultitapePart, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException;

    /**
     * Deserializes an element from the given input stream
     * @param is inputstream to read from
     * @param version ETFS version present in input stream (or -1 if unknown)
     * @param ignorePluginSections If true, the plugin data present on the tape should not be given to the plugins. Used
     *                             when reading multitape volumes, as the header parts need to be reread, but the plugins shouldn't
     *                             get any information about this to avoid reconfiguration to a wrong tape
     * @param context etfs instance as context for global methods
     * @param callback Callback given by the user
     * @return New InputStream to use (Is changed when changing tapes)
     * @throws IOException
     */
    public InputStream deserialize(InputStream is, int version, boolean ignorePluginSections, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException;
}
